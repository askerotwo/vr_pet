﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForcedThrowingSnowBall : MonoBehaviour
{
    [SerializeField] private float acceleration = 100.0f;
    private Rigidbody rigidBody;
    private GameObject rightHand;
    private SphereCollider sphereCollider;

    private bool isActive = false;

    private void Start()
    {
        rigidBody = gameObject.GetComponent<Rigidbody>();
        sphereCollider = gameObject.GetComponent<SphereCollider>();
    }

    public void activateSnowBall()
    {
        rightHand = GameObject.Find("PlayerVR/TrackedAlias/Aliases/RightControllerAlias");
        isActive = true;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(2) && isActive)
        {
            Debug.LogError(rightHand.transform.forward);
            sphereCollider.enabled = true;
            rigidBody.AddForce(rightHand.transform.forward * acceleration, ForceMode.Acceleration);
            isActive = false;
        }
    }
}
