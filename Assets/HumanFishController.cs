﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanFishController : MonoBehaviour
{
    private Vector3 direction;
    public float acceleration;
    public Rigidbody rb;
    private bool start = true;

    private GameObject penguin;
    private GameObject snowballposition;
    private GameObject human;
    private HumanAI humanai;



    private void Start()
    {
        penguin = GameObject.Find("Penguin");
        human = GameObject.Find("Human");
        humanai = human.GetComponent<HumanAI>();
        snowballposition = GameObject.Find("initialSnowBallPosition");
    }

    private void FixedUpdate()
    {
        if (start && humanai._isThrowFish)
        {

            Vector3 inititalSnowBallPosition = snowballposition.transform.position;
            Vector3 penguinPosition = penguin.transform.position;
            Debug.LogError(penguinPosition);
            direction = penguinPosition - inititalSnowBallPosition;
            direction = direction.normalized;
            //direction.y += 0.3f;
            rb.AddForce(direction * acceleration, ForceMode.Impulse);
            start = false;
            StartCoroutine(destroyFish());
        }
        else if (start && !humanai._isThrowFish)
        {
            Debug.LogError("Рыба летит в пингвина!");
            direction = Vector3.down;
            acceleration = 5;
            direction = direction + penguin.transform.right;
            direction += new Vector3(0, 15, 0);
            rb.AddForce(direction.normalized * acceleration, ForceMode.Impulse);
            start = false;
            StartCoroutine(destroyFish());
        }
    }

    IEnumerator destroyFish()
    {
        yield return new WaitForSeconds(2.0f);
        Destroy(this.gameObject);
    }
}
