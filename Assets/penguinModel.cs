﻿using Valve.Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.IO;
using UnityEngine;


public class penguinModel : MonoBehaviour
{
    const double r = 1e-1;
    const double criticalValueForDiffNorms = 0.32;
    private const double r1 = 0.3;
    private static bool unstableRelations = true;
    private penguinAI penguinai;
    public static string JSON_PATH_ALL_ACTIONS = Application.streamingAssetsPath + "\\Actions.json";
    public static string JSON_PATH_INDEPENDENT_PENGUIN_ACTIONS = Application.streamingAssetsPath + "\\IndependentPenguinActions.json";
    private string JSON_PATH_INDEPENDENT_FEELINGS_STATES = Application.streamingAssetsPath + "\\FeelingsStates.json";

    static int countHumanActions = 0;
    static bool processRecoveryOfFeelings = false;
    static string humanCharacteristic = "NAN";
    static string penguinCharacteristic = "NAN";

    public Dictionary<string, Act> allActs = new Dictionary<string, Act>();
    static Dictionary<string, Act> allIndependentActions = new Dictionary<string, Act>();
    static Dictionary<string, FeelingState> feelingsStates = new Dictionary<string, FeelingState>();
    static List<Tuple<string, double>> somaticLikelihood = new List<Tuple<string, double>>();
    public List<Tuple<string, double>> biasLikelihood = new List<Tuple<string, double>>();

    static public double[] penguinAppraisals = new double[3];
    static public double[] humanAppraisals = new double[3];
    static public double[] penguinFeelings = new double[3];
    static public double[] humanFeelings = new double[3];
    static public double[] penguinSomaticFactor = new double[5];

    public double TimerToEat = 0;
    public bool timetoeat = false;
    public bool timetoplay = false;
    public double TimerToPlay = 0;

    private const double k = 0.1;

    private void Start()
    {
        penguinai = GetComponent<penguinAI>();
    }
    public double[] getPenguinAppraisals()
    {
        return penguinAppraisals;
    }
    public double[] getPenguinBodyFactor()
    {
        return penguinSomaticFactor;
    }

    public double[] getPenguinFeelings()
    {
        return penguinFeelings;
    }
    public double[] getHumanAppraisals()
    {
        return humanAppraisals;
    }
    public double[] getHumanFeelings()
    {
        return humanFeelings;
    }

    public string getHumanCharacteristic()
    {
        return humanCharacteristic;
    }
    private void Update()
    {
        Debug.Log("pain = " + penguinSomaticFactor[(int)body.pain]);
        Debug.Log("energy = " + penguinSomaticFactor[(int)body.energy]);
        Debug.Log("satiety = " + penguinSomaticFactor[(int)body.satiety]);
        Debug.Log("activity = " + penguinSomaticFactor[(int)body.activity]);
        Debug.Log("sociability = " + penguinSomaticFactor[(int)body.sociability]);
    }

    private void FixedUpdate()
    {
        
        //for (int i = 0; i < 5; i++)
        //{
        //    if (penguinSomaticFactor[0] < 0.02)
        //        penguinSomaticFactor[0] = 0;
        //    if (!penguinai.Stop && !penguinai.isSleep)
        //        penguinSomaticFactor[i] -= 0.0001;

        //}


        foreach (var el in penguinSomaticFactor)
        {
           // Debug.Log(el);
        }

        if (penguinSomaticFactor[(int)body.energy] < -0.8)
        {
            if (penguinai.isSleep == false)
            {
                //penguinMakeAct(GO_SLEEP_PENGUIN);
                penguinai.PenguinActions("goSleepPenguin");
            }

        }

        if (penguinSomaticFactor[(int)body.satiety] > -0.3)
            timetoeat = false;

        if (timetoeat)
            TimerToEat += 0.02;
        else
            TimerToEat = 0;



        if (penguinSomaticFactor[(int)body.satiety] < -0.3 && TimerToEat % 15 < 0.5)
        {
            timetoeat = true;
            if (penguinai.Stop == false)
            {
                //penguinMakeAct(GO_EAT_PENGUIN);
                penguinai.PenguinActions("goToBoxWithFish");
            }

        }

        if (penguinSomaticFactor[(int)body.activity] > -0.25)
            timetoplay = false;

        if (timetoplay)
            TimerToPlay += 0.02;
        else
            TimerToPlay = 0;


        if (penguinSomaticFactor[(int)body.activity] < -0.25 && TimerToPlay % 20 < 0.5)
        {
            timetoplay = true;
            if (penguinai.Stop == false)
            {
                //penguinMakeAct(GO_PLAY_PENGUIN);
                penguinai.PenguinActions("goToBoxWithSnowBalls");
            }

        }

        if (penguinSomaticFactor[(int)body.sociability] < -0.15)
        {
            if (penguinai.Stop == false)
            {
                //penguinMakeAct(GO_TO_COMMUNICATE);
                penguinai.PenguinActions("GoCommunicatePenguin");
            }

        }

    }

    enum body
    {
        pain = 0,
        energy = 1,
        satiety = 2,
        activity = 3,
        sociability = 4
    };

    enum moral
    {
        valence = 0,
        arousal = 1,
        dominance = 2
    };

    public class FeelingState
    {
        public double[] feelingState;

        FeelingState()
        {
            feelingState = new double[3];
        }

        public void setActionAuthor(double[] inputFeelingState)
        {
            inputFeelingState.CopyTo(this.feelingState, 0);
        }

        public double[] getBodyFactorForTarget()
        {
            return feelingState;
        }
    }

    public class Act
    {
        public double[] bodyFactorForTarget;
        public double[] moralFactorForTarget;
        public double[] moralFactorForAuthor;
        public string name;
        public string responseActionOn;
        public string actionAuthor;

        public Act()
        {
            bodyFactorForTarget = new double[5];
            moralFactorForTarget = new double[3];
            moralFactorForAuthor = new double[3];
        }
        public void setBodyFactorForTarget(double[] values)
        {
            values.CopyTo(bodyFactorForTarget, 0);
        }
        public void setMoralFactorForTarget(double[] values)
        {
            values.CopyTo(moralFactorForTarget, 0);
        }
        public void setMoralFactorForAuthor(double[] values)
        {
            values.CopyTo(moralFactorForAuthor, 0);
        }

        public void setName(string name)
        {
            this.name = name;
        }
        public void setResponseActionOn(string responseActionOn)
        {
            this.responseActionOn = responseActionOn;
        }

        public void setActionAuthor(string actionAuthor)
        {
            this.actionAuthor = actionAuthor;
        }

        public double[] getBodyFactorForTarget()
        {
            return bodyFactorForTarget;
        }
        public double[] getMoralFactorForTarget()
        {
            return moralFactorForTarget;
        }

        public double[] getMoralFactorForAuthor()
        {
            return moralFactorForAuthor;
        }

        public string getName()
        {
            return name;
        }

        public string getResponseActionOn()
        {
            return responseActionOn;
        }

        public string getActionAuthor()
        {
            return actionAuthor;
        }

    };

    static double[] getNewSomaticFactor(double[] body, double[] action)
    {
        double[] resultBody = new double[body.Length];
        for (int i = 0; i < body.Length; ++i)
        {
            resultBody[i] = body[i] + action[i];
        }
        return resultBody;
    }

    public double[] getNewAppraisals(double[] appraisals, double[] action)
    {
        double[] resultAppraisals = new double[appraisals.Length];
        for (int i = 0; i < appraisals.Length; ++i)
        {
            resultAppraisals[i] = (1.0 - r) * appraisals[i] + r * action[i];
        }
        return resultAppraisals;
    }

    public void penguinMakeAct(string action)
    {
        penguinAppraisals = getNewAppraisals(penguinAppraisals, allIndependentActions[action].getMoralFactorForTarget());
        humanAppraisals = getNewAppraisals(humanAppraisals, allIndependentActions[action].getMoralFactorForAuthor());
        penguinSomaticFactor = getNewSomaticFactor(penguinSomaticFactor, allIndependentActions[action].getBodyFactorForTarget());
    }
    class Penguin
    {
        private double[] bodyFactor;
        private double[] moralFactor;
        public Penguin()
        {
            bodyFactor = new double[5];
            moralFactor = new double[3];
        }

    };


    public void setupActs()
    {
        feelingsStates = JsonConvert.DeserializeObject<Dictionary<string, FeelingState>>(File.ReadAllText(JSON_PATH_INDEPENDENT_FEELINGS_STATES));
        allActs = JsonConvert.DeserializeObject<Dictionary<string, Act>>(File.ReadAllText(JSON_PATH_ALL_ACTIONS));
        allIndependentActions = JsonConvert.DeserializeObject<Dictionary<string, Act>>(File.ReadAllText(JSON_PATH_INDEPENDENT_PENGUIN_ACTIONS));
    }

    void isRelationsUnstable(double[] feelings)
    {
        string humanCharacteristic = findHumanFeelingCharacteristick(feelings);
        double dif = 10;
        for (int i = 0; i < 3; ++i)
        {
            if (feelingsStates[humanCharacteristic].feelingState[i] != 0)
            {
                dif = Math.Min(Math.Abs(feelings[i] - feelingsStates[humanCharacteristic].feelingState[i]), dif);
            }
        }
        if (dif < 0.15)
        {
            unstableRelations = false;
        }
    }

    double[] getNewFeelingFactor(double[] feelings, double[] appraisals)
    {
        isRelationsUnstable(feelings);
        if (unstableRelations)
        {
            feelings = firstMethorRebuildFeelings(feelings, appraisals);
            return feelings;
        }
        double diffNorm = 0;
        for (int i = 0; i < 3; ++i)
        {
            diffNorm += Math.Abs(feelings[i] - appraisals[i]);
        }
        processRecoveryOfFeelings = diffNorm > criticalValueForDiffNorms;
        if (processRecoveryOfFeelings)
        {
            feelings = secondMethodRebuildFeelings(feelings, appraisals);
        }
        else
        {
            feelings = setConstantFeelings(feelings);
        }
        return feelings;
    }

    double[] firstMethorRebuildFeelings(double[] feelings, double[] appraisals)
    {
        double[] resultFeelings = new double[feelings.Length];
        for (int i = 0; i < 3; i++)
        {
            resultFeelings[i] = 1.1 * appraisals[i];
        }
        return resultFeelings;
    }

    double[] secondMethodRebuildFeelings(double[] feelings, double[] appraisals)
    {
        Console.WriteLine("secondMethodRebuildFeelings");
        double[] resultFeelings = new double[feelings.Length];
        for (int i = 0; i < 3; ++i)
        {
            resultFeelings[i] = (1 - r1) * feelings[i] + r1 * (appraisals[i] - feelings[i]);
        }
        humanCharacteristic = "NAN";
        return resultFeelings;
    }

    string findHumanFeelingCharacteristick(double[] feelings)
    {
        string choise = "";
        double dif = 20;
        foreach (var feelingMassive in feelingsStates)
        {
            for (int i = 0; i < 3; ++i)
            {
                if (feelingMassive.Value.feelingState[i] != 0)
                {
                    double mid = Math.Abs(feelingMassive.Value.feelingState[i] - feelings[i]);
                    if (mid < dif)
                    {
                        dif = mid;
                        choise = feelingMassive.Key;
                    }
                }
            }
        }
        return choise;
    }

    double[] setConstantFeelings(double[] feelings)
    {
        Console.WriteLine("setConstantFeelings");
        humanCharacteristic = findHumanFeelingCharacteristick(feelings);
        double[] ans = new double[3];
        feelingsStates[humanCharacteristic].feelingState.CopyTo(ans, 0);
        return ans;
    }
    public void somaticCriterion(double[] penguinBodyFactor, string action)
    {
        double maxValue = 0;
        double mainNorm = 0;
        double recNorm = 0;
        foreach (var characterAction in allActs)
        {
            double normAfterAction = 0;
            var currentAction = characterAction.Value;
            if (currentAction.getResponseActionOn() == action)
            {
                for (int i = 0; i < penguinBodyFactor.Length; ++i)
                {
                    normAfterAction += Math.Pow(penguinBodyFactor[i] + currentAction.getBodyFactorForTarget()[i], 2);
                }
                somaticLikelihood.Add(new Tuple<string, double>(currentAction.getName(), normAfterAction));
                mainNorm += normAfterAction;
                maxValue = Math.Max(maxValue, Math.Abs(normAfterAction));
            }
        }
        for (int i = 0; i < somaticLikelihood.Count; ++i)
        {
            somaticLikelihood[i] = new Tuple<string, double>(somaticLikelihood[i].Item1, 1 - somaticLikelihood[i].Item2 / mainNorm);
            recNorm += somaticLikelihood[i].Item2;
            //somaticLikelihood[i] = new Tuple<string, double>(somaticLikelihood[i].Item1, maxValue - somaticLikelihood[i].Item2);
        }
        for (int i = 0; i < somaticLikelihood.Count; ++i)
        {
            somaticLikelihood[i] = new Tuple<string, double>(somaticLikelihood[i].Item1, somaticLikelihood[i].Item2 / recNorm);
            System.Console.WriteLine("somatic norms for " + somaticLikelihood[i].Item1 + " " + somaticLikelihood[i].Item2);
        }
    }

    public void biasCriterion(double[] appraisalsFactor, double[] feelingsFactor, string action)
    {
        double maxValue = 0;
        double mainNorm = 0;
        double recNorm = 0;
        foreach (var el in allActs)
        {
            double difference = 0;
            var responseAction = el.Value;
            if (responseAction.getResponseActionOn() == action)
            {
                var appraisalsAfterAction = getNewAppraisals(appraisalsFactor, responseAction.getBodyFactorForTarget());
                for (int i = 0; i < feelingsFactor.Length; ++i)
                {
                    difference += Math.Pow(feelingsFactor[i] - appraisalsAfterAction[i], 2);
                }
                biasLikelihood.Add(new Tuple<string, double>(responseAction.getName(), difference));
                mainNorm += difference;
                maxValue = Math.Max(maxValue, difference);
            }
        }
        for (int i = 0; i < biasLikelihood.Count; ++i)
        {
            biasLikelihood[i] = new Tuple<string, double>(biasLikelihood[i].Item1, 1 - biasLikelihood[i].Item2 / mainNorm);
            recNorm += biasLikelihood[i].Item2;
            // biasLikelihood[i] = new Tuple<string, double>(biasLikelihood[i].Item1, maxValue - biasLikelihood[i].Item2);
        }
        for (int i = 0; i < biasLikelihood.Count; ++i)
        {
            biasLikelihood[i] = new Tuple<string, double>(biasLikelihood[i].Item1, biasLikelihood[i].Item2 / recNorm);
            System.Console.WriteLine("bias norms for " + biasLikelihood[i].Item1 + " " + biasLikelihood[i].Item2);
        }
    }

    void rebuildStatesAfterHumanAction(string humanAct)
    {
        humanAppraisals = getNewAppraisals(humanAppraisals, allActs[humanAct].getMoralFactorForAuthor()); // изменения состояния человека после действия
        humanFeelings = getNewFeelingFactor(humanFeelings, humanAppraisals);

        penguinAppraisals = getNewAppraisals(penguinAppraisals, allActs[humanAct].getMoralFactorForTarget());
        penguinSomaticFactor = getNewSomaticFactor(penguinSomaticFactor, allActs[humanAct].getBodyFactorForTarget());
        //penguinFeelings = getNewFeelingFactor(penguinFeelings, penguinAppraisals); //изменения состояния пингвина после действия
        System.Console.WriteLine("humanCharacteristic = " + humanCharacteristic);
        mitAppraisalsAndFeelings();
    }

    void rebuildStatesAfterPenguinAction(string penguinAct)
    {
        humanAppraisals = getNewAppraisals(humanAppraisals, allActs[penguinAct].getMoralFactorForTarget()); // изменения состояния человека после действия
        humanFeelings = getNewFeelingFactor(humanFeelings, humanAppraisals);

        penguinAppraisals = getNewAppraisals(penguinAppraisals, allActs[penguinAct].getMoralFactorForAuthor());
        penguinSomaticFactor = getNewSomaticFactor(penguinSomaticFactor, allActs[penguinAct].getBodyFactorForTarget());
        //penguinFeelings = getNewFeelingFactor(penguinFeelings, penguinAppraisals); //изменения состояния пингвина после действия
        System.Console.WriteLine("humanCharacteristic = " + humanCharacteristic);
        mitAppraisalsAndFeelings();
    }

    public string getResponseAction()
    {
        string response = "";
        List<Tuple<string, double>> result = new List<Tuple<string, double>>();
        foreach (var somaticEl in somaticLikelihood)
        {
            foreach (var biasEl in biasLikelihood)
            {
                if (somaticEl.Item1 == biasEl.Item1)
                {
                    result.Add(new Tuple<string, double>(somaticEl.Item1, biasEl.Item2 + somaticEl.Item2 * k));
                }
            }
        }
        double actionLikelihood = 0;
        foreach (var el in result)
        {
            if (el.Item2 > actionLikelihood)
            {
                actionLikelihood = el.Item2;
                response = el.Item1;
            }
        }
        return response;
    }



    public string getResponseActionByLikelihood()
    {
        string response = "";
        List<Tuple<string, double>> result = new List<Tuple<string, double>>();
        double sum = 0;
        foreach (var somaticEl in somaticLikelihood)
        {
            foreach (var biasEl in biasLikelihood)
            {
                if (somaticEl.Item1.Equals(biasEl.Item1))
                {
                    double likelihood = biasEl.Item2 + somaticEl.Item2 * k;
                    result.Add(new Tuple<string, double>(somaticEl.Item1, likelihood));
                    sum += likelihood;
                }
            }
        }
        for (int i = 0; i < result.Count; ++i)
        {
            result[i] = new Tuple<string, double>(result[i].Item1, result[i].Item2 / sum);
        }
        result.Sort((x1, y1) => x1.Item2.CompareTo(y1.Item2));
        System.Random x = new System.Random();
        double actionLikelihood = Convert.ToDouble(x.Next(0, 10000) / 10000.0);
        double currentLikelihood = 0;
        foreach (var el in result)
        {
            currentLikelihood += el.Item2;
            if (currentLikelihood > actionLikelihood)
            {
                response = el.Item1;
                break;
            }
        }
        return response;
    }

    double[] limitVecs(double[] vec)
    {
        for (int i = 0; i < vec.Length; ++i)
        {
            vec[i] = Math.Min(vec[i], 0.5);
            vec[i] = Math.Max(vec[i], -0.5);
        }
        return vec;
    }

    void mitAppraisalsAndFeelings()
    {
        limitVecs(humanAppraisals).CopyTo(humanAppraisals, 0);
        limitVecs(humanFeelings).CopyTo(humanFeelings, 0);
        limitVecs(penguinAppraisals).CopyTo(penguinAppraisals, 0);
        limitVecs(penguinFeelings).CopyTo(penguinFeelings, 0);
    }

    public string launchNewScene(string humanAct)
    {
        somaticLikelihood = new List<Tuple<string, double>>();
        biasLikelihood = new List<Tuple<string, double>>();

        rebuildStatesAfterHumanAction(humanAct);
        somaticCriterion(penguinSomaticFactor, humanAct);
        biasCriterion(humanAppraisals, humanFeelings, humanAct);
        string answer = getResponseActionByLikelihood();
        rebuildStatesAfterPenguinAction(answer);
        //Debug.LogError("RESULTED ACTION " + answer);
        //Debug.LogError("HUMAN CHARACTERICTIC " + humanCharacteristic);
        return answer;
    }


    //public void printAllLogs(string reaction)
    //{
    //    System.Console.WriteLine("Penguin reaction = " + reaction);
    //    System.Console.WriteLine("humanCharacteristic = " + humanCharacteristic);
    //    System.Console.Write("penguinAppraisals ");
    //    printArray(penguinAppraisals);
    //    System.Console.Write("humanAppraisals ");
    //    printArray(humanAppraisals);
    //    System.Console.Write("penguinFeelings ");
    //    printArray(penguinFeelings);
    //    System.Console.Write("humanFeelings ");
    //    printArray(humanFeelings);
    //    System.Console.Write("penguinBodyFactor ");
    //    printArray(penguinSomaticFactor);

    //    System.Console.WriteLine(getResponseAction());
    //    foreach (var el in somaticLikelihood)
    //    {
    //        System.Console.WriteLine(el.Item1 + " " + el.Item2);
    //    }
    //    System.Console.WriteLine();
    //    foreach (var el in biasLikelihood)
    //    {
    //        System.Console.WriteLine(el.Item1 + " " + el.Item2);
    //    }
    //}

    //public penguinModel()
    //{
    //    string[] arr = new string[6];
    //    arr[0] = HELLO_PINGUIN;
    //    arr[1] = FEED_PINGUIN;
    //    arr[2] = THROW_BALL_TO_PINGUIN;
    //    arr[3] = STROKE_PINGUIN;
    //    arr[4] = HIT_PINGUIN;
    //    arr[5] = "EXIT";
    //    setupActs();
    //    while (true)
    //    {
    //        for (int i = 0; i < arr.Length; ++i)
    //        {
    //            System.Console.WriteLine(i + " " + arr[i]);
    //        }
    //        System.Console.WriteLine();
    //        string inputAction;
    //        inputAction = System.Console.ReadLine();
    //        int value = Convert.ToInt32(inputAction);
    //        System.Console.WriteLine(inputAction);
    //        if (value == arr.Length - 1)
    //        {
    //            break;
    //        }
    //        string reaction = gettedExternalEffect(arr[value]);
    //        printAllLogs(reaction);
    //    }
    //}
}