﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class forcedThrowingWithKeyBoard : MonoBehaviour
{
    [Range(10, 20)]
    [SerializeField] private float acceleration;

    private GameObject rightHand;
    private bool isActive = false;

    public void activateItem()
    {
        rightHand = GameObject.Find("PlayerVR/TrackedAlias/Aliases/RightControllerAlias");
        isActive = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && isActive)
        {
            StartCoroutine("throwItemCoroutine");
        }
    }

    public void StartDestroy()
    {
        StartCoroutine(destroy());
    }

    private IEnumerator destroy()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }

    private IEnumerator throwItemCoroutine()
    {
        yield return new WaitForSeconds(0.1f);
        throwItem();
    }

    private void throwItem()
    {
        Rigidbody rigidBody = this.gameObject.GetComponent<Rigidbody>();
        rigidBody.isKinematic = false;
        rigidBody.AddForce(rightHand.transform.forward * acceleration, ForceMode.Impulse);
        isActive = false;
    }
}
