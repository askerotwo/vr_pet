﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.UI;

public class HumanAI : MonoBehaviour
{
    private NavMeshAgent agent;
    [SerializeField] public float _maxDistance = 10;
    public GameObject OriginPosition;
    private Vector3 _originPosition;
    private Vector3 _lastDestination;
    private float _stayTime = 5.0f;

    private int rand = 0;
    [SerializeField] public GameObject BasketFish;
    private Vector3 BasketFishPosition;
    [SerializeField] public GameObject BasketSnowBall;
    private Vector3 BasketSnowBallPosition;
    [SerializeField] private Text humantext;
    [SerializeField] public GameObject Penguin;
    [SerializeField] public GameObject PenguinPosition;
    [SerializeField] private GameObject snowBall;
    [SerializeField] private GameObject InitialSnowBallPosition;
    private GameObject _snowball;
    [SerializeField] private GameObject fish;
    private GameObject _fish;
    private AudioSource _humanAudioSource;
    private Vector3 _penguinLastPosition;
    [SerializeField] private AudioClip clipHelloPenguin;
    private penguinAI penguinai;
    private LoggingFile penguinloggingfile;
    private penguinModel penguinmodel;
    private HumanModel humanmodel;
    public Animator animator;
    private float timer = 0;
    private bool lookatpenguin = false;

    private float distWalking;
    private float distThrowFish;
    private float distThrowSnowBall;
    private float distStrokePenguin;
    private float distHitPenguin;

    private bool isCoroutineExecutingStroke = false;
    private bool isCoroutineExecutingHit = false;
    private bool _isCoroutineExecuting;
    private bool isCoroutineExecutingThrowSnowBall = false;
    private bool isCoroutineExecutingThrowFish = false;

    public bool _isThrowFish = false;
    public bool _isThrowSnowBall = false;
    private bool _isStrokePenguin = false;
    private bool _isHitPenguin = false;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        BasketFishPosition = BasketFish.transform.position;
        BasketSnowBallPosition = BasketSnowBall.transform.position;
        _humanAudioSource = GetComponent<AudioSource>();
        _originPosition = OriginPosition.transform.position;
        penguinai = Penguin.GetComponent<penguinAI>();
        penguinmodel = Penguin.GetComponent<penguinModel>();
        penguinloggingfile = Penguin.GetComponent<LoggingFile>();
        humanmodel = GetComponent<HumanModel>();
    }


    private void FixedUpdate()
    {
        if (lookatpenguin)
        {
            this.transform.LookAt(Penguin.transform);
        }
    }

    void Update()
    {
        timer += Time.deltaTime;
        Debug.Log("timer = " + timer.ToString());
        if(timer > 15)
        {
            string action = humanmodel.defineAction();
            Debug.LogError(action);
            HumanActions(action);
            timer = 0f;
        }
        // 
        if (Input.GetKeyDown(KeyCode.J))
        {
            Move(OriginPosition.transform.position);
        }
        // Человек подходит к корзине с рыбой и "бросает" рыбу пингвину
        //if (Input.GetKeyDown(KeyCode.T))
        //{
        //    penguinai.stopAllIndependentActions();
        //    _isCoroutineExecuting = false;
        //    Move(BasketFishPosition);
        //    _isThrowFish = true;
        //}
        // Человек подходит к корзине с снежками и "бросает" снежок пингвину
        if (Input.GetKeyDown(KeyCode.Y))
        {
            penguinai.stopAllIndependentActions();
            _isCoroutineExecuting = false;
            Move(BasketSnowBallPosition);
            _isThrowSnowBall = true;
        }

        // Человек подходит к пингвину и "гладит" его
        if (Input.GetKeyDown(KeyCode.U))
        {
            penguinai.stopAllIndependentActions();
            _isCoroutineExecuting = false;
            agent.SetDestination(transform.position);
            _isStrokePenguin = true;
            _penguinLastPosition = PenguinPosition.transform.position;
            Move(PenguinPosition.transform.position);
            Debug.Log("Бегу за пингвином!");
        }

        // Человек подходит к пингвину и "бьет" его
        if (Input.GetKeyDown(KeyCode.I))
        {
            penguinai.stopAllIndependentActions();
            _isCoroutineExecuting = false;
            agent.SetDestination(transform.position);
            _isHitPenguin = true;
            _penguinLastPosition = PenguinPosition.transform.position;
            Move(PenguinPosition.transform.position);
            Debug.Log("Бегу бить пингвина!");
        }


        // Человек приветсвтует пингвина
        if (Input.GetKeyDown(KeyCode.F))
        {
            StartCoroutine(Waving());
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            StartCoroutine(Stay());
        }

        distWalking = (agent.pathPending)
            ? Vector3.Distance(transform.position, _lastDestination)
            : agent.remainingDistance;
        distThrowFish = (agent.pathPending && _isThrowFish)
            ? Vector3.Distance(transform.position, BasketFishPosition)
            : agent.remainingDistance;
        distThrowSnowBall = (agent.pathPending && _isThrowSnowBall)
            ? Vector3.Distance(transform.position, BasketSnowBallPosition)
            : agent.remainingDistance;
        distStrokePenguin = (agent.pathPending && _isStrokePenguin)
            ? Vector3.Distance(transform.position, PenguinPosition.transform.position)
            : agent.remainingDistance;
        distHitPenguin = (agent.pathPending && _isHitPenguin)
            ? Vector3.Distance(transform.position, PenguinPosition.transform.position)
            : agent.remainingDistance;

        if ((_isStrokePenguin || _isHitPenguin) && Vector3.Distance(_penguinLastPosition, PenguinPosition.transform.position) > 2)
        {
            _penguinLastPosition = PenguinPosition.transform.position;
            Move(PenguinPosition.transform.position);
        }

        if (distThrowFish < 0.2 && _isThrowFish && !isCoroutineExecutingThrowFish)
        {
            StartCoroutine(throwFish());
        }


        if (distThrowSnowBall < 0.2 && _isThrowSnowBall && !isCoroutineExecutingThrowSnowBall)
        {
            StartCoroutine(ThrowSnowBall());
        }
        if (distStrokePenguin < 2 && !isCoroutineExecutingStroke && _isStrokePenguin)
        {
            StartCoroutine(StrokePenguinCoroutine());
        }

        if (distHitPenguin < 2 && !isCoroutineExecutingHit && _isHitPenguin)
        {
            StartCoroutine(HitPenguinCoroutine());
        }



        if (!_isCoroutineExecuting && distWalking < 0.1)
        {
            rand = UnityEngine.Random.Range(0, 25);
            if (rand > 0 && rand < 15)
            {
                Debug.Log("Move");
                Move();
            }
            else if (rand > 15 && rand < 25)
            {
                Debug.Log("Stay");
                StartCoroutine(Stay());
            }
        }
        Debug.Log("distWalking = " + distWalking.ToString());
        Debug.Log("rand = " + rand.ToString());
    }

    void Move()
    {
        animator.SetInteger("State", 0);
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * _maxDistance;
        randomDirection += _originPosition;
        NavMeshHit navHit;
        NavMesh.SamplePosition(randomDirection, out navHit, _maxDistance, -1);
        _lastDestination = navHit.position;
        agent.SetDestination(navHit.position);
    }

    public void Move(Vector3 pos)
    {
        animator.SetInteger("State", 0);
        NavMeshHit navHit;
        NavMesh.SamplePosition(pos, out navHit, _maxDistance, -1);
        _lastDestination = navHit.position;
        agent.SetDestination(navHit.position);
    }

    IEnumerator Stay()
    {
        animator.SetInteger("State", 1);
        agent.SetDestination(transform.position);
        if (_isCoroutineExecuting)
            yield break;
        _isCoroutineExecuting = true;
        //Время , которое пингвин стоит на месте. 
        yield return new WaitForSeconds(_stayTime);
        _isCoroutineExecuting = false;
    }

    IEnumerator StrokePenguinCoroutine()
    {
        humantext.text = "Человек гладит пингвина.";
        penguinai.penguinstayforhuman();
        _isCoroutineExecuting = true;
        if (isCoroutineExecutingStroke)
            yield break;
        isCoroutineExecutingStroke = true;
        lookatpenguin = true;
        animator.SetInteger("State", 3);
        yield return new WaitForSeconds(3.0f);
        penguinai.stopAllIndependentActions();
        if (!penguinai.stopContinuity)
        {
            string action = penguinmodel.launchNewScene("Human strokes penguin");
            penguinai.PenguinActions(action);
        }
        Debug.LogError("Человек \"погладил\" пингвина");
        isCoroutineExecutingStroke = false;
        humantext.text = "";
        lookatpenguin = false;
        _isCoroutineExecuting = false;
        _isStrokePenguin = false;
    }
    IEnumerator throwFish()
    {
        humantext.text = "Человек бросает рыбу пингвину.";
        isCoroutineExecutingThrowFish = true;
        _isCoroutineExecuting = true;
        this.transform.LookAt(Penguin.transform);
        animator.SetInteger("State", 2);
        yield return new WaitForSeconds(1.0f);
        _fish = Instantiate(fish) as GameObject;
        Vector3 fishPosition = InitialSnowBallPosition.transform.position;
        _fish.transform.position = fishPosition;
        _fish.transform.rotation = transform.rotation;
        yield return new WaitForSeconds(1.5f);
        penguinloggingfile.GettedNewAction("Human", "No target", "Человек взял в руки рыбу", "0");
        penguinai.stopAllIndependentActions();
        if (!penguinai.stopContinuity)
        {
            string action = penguinmodel.launchNewScene("Human feeds penguin");
            penguinai.PenguinActions(action);
            penguinai.waitingForFish = false;
            Debug.Log("FishHit from fish");
        }
        Debug.Log("Человек бросил рыбу пингвину!");
        humantext.text = "";
        _isThrowFish = false;
        isCoroutineExecutingThrowFish = false;
        _isCoroutineExecuting = false;
    }
    IEnumerator ThrowSnowBall()
    {
        humantext.text = "Человек бросает снежок пингвину.";
        isCoroutineExecutingThrowSnowBall = true;
        _isCoroutineExecuting = true;
        this.transform.LookAt(Penguin.transform);
        animator.SetInteger("State", 2);
        yield return new WaitForSeconds(1.1f);
        _snowball = Instantiate(snowBall) as GameObject;
        Vector3 snowBallPosition = InitialSnowBallPosition.transform.position;
        _snowball.transform.position = snowBallPosition;
        _snowball.transform.rotation = transform.rotation;
        yield return new WaitForSeconds(1.5f);
        penguinloggingfile.GettedNewAction("Human", "No target", "Человек взял в руки снежок", "0");
        penguinai.stopAllIndependentActions();
        if (!penguinai.stopContinuity)
        {
            string action = penguinmodel.launchNewScene("Human throws ball into penguin");
            penguinai.PenguinActions(action);
        }
        Debug.LogError("Человек бросил снежок пингвину!");
        _isThrowSnowBall = false;
        humantext.text = "";
        isCoroutineExecutingThrowSnowBall = false;
        _isCoroutineExecuting = false;
    }
    IEnumerator HitPenguinCoroutine()
    {
        humantext.text = "Человек ударил пингвина.";
        _isCoroutineExecuting = true;
        penguinai.penguinstayforhuman();
        _isCoroutineExecuting = true;
        if (isCoroutineExecutingHit)
            yield break;
        isCoroutineExecutingHit = true;
        //lookatpenguin = true;
        this.transform.LookAt(Penguin.transform);
        animator.SetInteger("State", 5);
        yield return new WaitForSeconds(0.6f);
        penguinai.stopAllIndependentActions();
        if (!penguinai.stopContinuity)
        {
            string action = penguinmodel.launchNewScene("Human hits penguin");
            penguinai.PenguinActions(action);
        }
        Debug.LogError("Человек ударил пингвина");
        Debug.LogError("Человек остановился после удара");
        StartCoroutine(Stay());
        isCoroutineExecutingHit = false;
        humantext.text = "";
        _isCoroutineExecuting = false;
        _isHitPenguin = false;
    }

    IEnumerator Waving()
    {
        humantext.text = "Человек приветствует пингвина.";
        Move(this.transform.position);
        animator.SetInteger("State", 4);
        _isCoroutineExecuting = true;
        Debug.LogError("Пыня привет!");
        this.transform.LookAt(Penguin.transform);
        penguinloggingfile.UpdateLogs("Human", "Penguin", "Человек приветствует пингвина", "0");
        Debug.LogError("Человек приветствует пингвина");
        penguinai.stopAllIndependentActions();
        _humanAudioSource.PlayOneShot(clipHelloPenguin);
        if (!penguinai.stopContinuity)
        {
            string action1 = penguinmodel.launchNewScene("Human greetings penguin");
            penguinai.PenguinActions(action1);
        }
        yield return new WaitForSeconds(1.0f);
        humantext.text = "";
        _isCoroutineExecuting = false;

    }

    public void HumanActions(String action)
    {
        if (action == "Human greetings penguin")
        {
            //penguinloggingfile.UpdateLogs("Human", "Penguin", "Человек приветствует пингвина", "0");
            //Debug.LogError("Человек приветствует пингвина");
            //penguinai.stopAllIndependentActions();
            //_humanAudioSource.PlayOneShot(clipHelloPenguin);
            //if (!penguinai.stopContinuity)
            //{
            //    string action1 = penguinmodel.launchNewScene("Human greetings penguin");
            //    penguinai.PenguinActions(action1);
            //}
            StartCoroutine(Waving());
        }

        if (action == "Human feeds penguin")
        {
            penguinloggingfile.UpdateLogs("Human", "Penguin", "Человек дает рыбу пингвину", "0");
            Debug.LogError("Человек дает рыбу пингвину");
            penguinai.stopAllIndependentActions();
            _isCoroutineExecuting = false;
            Move(BasketFishPosition);
            _isThrowFish = true;
        }

        if (action == "Human throws ball into penguin")
        {
            penguinloggingfile.UpdateLogs("Human", "Penguin", "Человек кидает снежок пингвину", "0");
            Debug.LogError("Человек кидает снежок пингвину");
            penguinai.stopAllIndependentActions();
            _isCoroutineExecuting = false;
            Move(BasketSnowBallPosition);
            _isThrowSnowBall = true;
        }
        if (action == "Human strokes penguin")
        {
            penguinloggingfile.UpdateLogs("Human", "Penguin", "Человек погладил пингвина", "0");
            Debug.Log("Человек погладил пингвина");
            penguinai.stopAllIndependentActions();
            _isCoroutineExecuting = false;
            agent.SetDestination(transform.position);
            _isStrokePenguin = true;
            _penguinLastPosition = PenguinPosition.transform.position;
            Move(PenguinPosition.transform.position);
            Debug.LogError("Бегу за пингвином!");
        }
        if (action == "Human hits penguin")
        {
            penguinloggingfile.UpdateLogs("Human", "Penguin", "Человек ударил пингвина", "0");
            Debug.Log("Человек погладил пингвина");
            penguinai.stopAllIndependentActions();
            _isCoroutineExecuting = false;
            agent.SetDestination(transform.position);
            _isHitPenguin = true;
            
            _penguinLastPosition = PenguinPosition.transform.position;
            Move(PenguinPosition.transform.position);
            Debug.LogError("Бегу бить пингвина!");
        }

    }

}

