﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFishTag : MonoBehaviour
{
    [SerializeField] private GameObject fish;
    [SerializeField] private GameObject penguin;
    public void SetTeg()
    {
        penguin = GameObject.Find("Penguin");
        penguin.GetComponent<LoggingFile>().GettedNewAction("Human", "Penguin", "Человек взял в руки рыбу", "0");
        penguin.GetComponent<LoggingFile>().GettedNewAction("Penguin", "Human", "Пингвин просит рыбу", "1");
        Debug.Log("HUMAN TAKE FISH");
        fish.gameObject.tag = "PlayerFish";
    }
}
