﻿using Valve.Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using UnityEngine;

public class HumanModel : MonoBehaviour
{


    [SerializeField] public GameObject Penguin;
    private penguinAI penguinai;
    private penguinModel penguinmodel;
    public List<Tuple<string, double>> biasLikelihood = new List<Tuple<string, double>>();
    [SerializeField] bool is_goal;
    [SerializeField] public double[] penguinfeelings;

    private void Start()
    {
        penguinai = Penguin.GetComponent<penguinAI>();
        penguinmodel = Penguin.GetComponent<penguinModel>();
        penguinfeelings = new double[3] { 0.5, 0, 0 };
    }


    public void biasCriterion()
    {
        double maxValue = 0;
        double mainNorm = 0;
        double recNorm = 0;
        foreach (var el in penguinmodel.allActs)
        {
            double difference = 0;
            var action = el.Value;
            if (action.actionAuthor.Equals("human"))
            {
                var appraisalsAfterAction = penguinmodel.getNewAppraisals(penguinmodel.getPenguinAppraisals(), action.getMoralFactorForTarget());
                for (int i = 0; i < penguinmodel.getPenguinFeelings().Length; ++i)
                {
                    //2-ой режим работы моральной схемы (цель установить дружеские отношения)
                    if (is_goal)
                    {
                        difference += Math.Pow(penguinfeelings[i] - appraisalsAfterAction[i], 2);
                        Debug.LogError("Choosing human action");
                    }
                    //Динамический режим работы моральной схемы (отсутствие заранее поставленной цели)
                    else
                    {
                        difference += Math.Pow(penguinmodel.getPenguinFeelings()[i] - appraisalsAfterAction[i], 2);
                    }
                }
                biasLikelihood.Add(new Tuple<string, double>(action.getName(), difference));
                mainNorm += difference;
                maxValue = Math.Max(maxValue, difference);
            }
        }
        for (int i = 0; i < biasLikelihood.Count; ++i)
        {
            biasLikelihood[i] = new Tuple<string, double>(biasLikelihood[i].Item1, 1 - biasLikelihood[i].Item2 / mainNorm);
            recNorm += biasLikelihood[i].Item2;
            // biasLikelihood[i] = new Tuple<string, double>(biasLikelihood[i].Item1, maxValue - biasLikelihood[i].Item2);
        }
        for (int i = 0; i < biasLikelihood.Count; ++i)
        {
            biasLikelihood[i] = new Tuple<string, double>(biasLikelihood[i].Item1, biasLikelihood[i].Item2 / recNorm);
            System.Console.WriteLine("bias norms for " + biasLikelihood[i].Item1 + " " + biasLikelihood[i].Item2);
        }
    }

    public string getResponseActionByLikelihood()
    {
        string response = "";
        List<Tuple<string, double>> result = new List<Tuple<string, double>>();
        double sum = 0;
        foreach (var biasEl in biasLikelihood)
        {
            double likelihood = biasEl.Item2;
            result.Add(new Tuple<string, double>(biasEl.Item1, likelihood));
            sum += likelihood;
        }
        for (int i = 0; i < result.Count; ++i)
        {
            result[i] = new Tuple<string, double>(result[i].Item1, result[i].Item2 / sum);
        }
        result.Sort((x1, y1) => x1.Item2.CompareTo(y1.Item2));
        System.Random x = new System.Random();
        double actionLikelihood = Convert.ToDouble(x.Next(0, 10000) / 10000.0);
        double currentLikelihood = 0;
        foreach (var el in result)
        {
            currentLikelihood += el.Item2;
            if (currentLikelihood > actionLikelihood)
            {
                response = el.Item1;
                break;
            }
        }
        return response;
    }

    public string defineAction()
    {
        biasLikelihood = new List<Tuple<string, double>>();
        biasCriterion();
        string answer = getResponseActionByLikelihood();
        // Debug.LogError("RESULTED ACTION " + answer);
        //Debug.LogError("HUMAN CHARACTERICTIC " + humanCharacteristic);
        return answer;
    }
}
