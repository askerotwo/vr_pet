﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class torchfireon : MonoBehaviour
{

    [SerializeField] public GameObject torchlight;
    [SerializeField] public GameObject torchsparks;

    // Start is called before the first frame update
    void Start()
    {
    }


    private void OnTriggerEnter(Collider other)
    {

        if(other.tag == "firetrigger")
        {
            torchlight.SetActive(true);
            torchsparks.SetActive(true);
        }
    }
}
