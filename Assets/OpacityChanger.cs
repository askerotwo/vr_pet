﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpacityChanger : MonoBehaviour
{
    public Material target;
    public void UpdateOpacity(float alphaValue)
    {
        Debug.LogError("Hello world");
        Color color = target.color;
        color.a = alphaValue;
        target.color = color;
    }
}
