﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fishDestroyOnSphere : MonoBehaviour
{
    [SerializeField] private GameObject penguin;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PenguinSphere" && this.tag == "PlayerFish")
        {
            penguin = GameObject.Find("Penguin");
            penguin.GetComponent<penguinAI>().stopAllIndependentActions();
            if (!penguin.GetComponent<penguinAI>().stopContinuity)
            {
                string action = penguin.GetComponent<penguinModel>().launchNewScene("Human feeds penguin");
                penguin.GetComponent<penguinAI>().PenguinActions(action);
                penguin.GetComponent<penguinAI>().waitingForFish = false;
                Debug.Log("FishHit from fish");
            }
            Destroy(this.gameObject);
        }
    }
}
