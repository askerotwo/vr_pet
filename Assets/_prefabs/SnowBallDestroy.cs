﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowBallDestroy : MonoBehaviour
{

    private GameObject spawningObjects;
    private SpawnSnawBalls spawnscript;

    private void Start()
    {
        spawningObjects = GameObject.Find("SpawningObjects");
        spawnscript = spawningObjects.GetComponent<SpawnSnawBalls>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<torchfireon>())
        {
            //Debug.Log("hit torch!");
            if(collision.gameObject.GetComponent<torchfireon>().torchlight.activeSelf)
            {
                Destroy(this.gameObject);
                spawnscript.countSnowBall -= 1;
            }

        }
    }

    public void StartDestroy()
    {
        StartCoroutine(destroy());
    }

    private IEnumerator destroy()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
        spawnscript.countSnowBall -= 1;
    }
}
