﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class setSnawBallTag : MonoBehaviour
{
    [SerializeField] private GameObject sphere;
    [SerializeField] private GameObject penguin;
    public void SetTeg()
    {
        penguin = GameObject.Find("Penguin");
        penguin.GetComponent<LoggingFile>().GettedNewAction("Human", "No target", "Человек взял в руки снежок", "0");
        Debug.Log("HUMAN TAKE SNOWBALL IN HAND");
        sphere.gameObject.tag = "PlayerSnowball";
    }
}
