﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanSnowBallController : MonoBehaviour
{
    private Vector3 direction;
    private Vector3 testdirection = new Vector3(0, 0, 1);
    public float acceleration;
    public Rigidbody rb;
    private bool start = true;

    private GameObject penguin;
    private GameObject snowballposition;
    private GameObject human;
    private HumanAI humanai;



    private void Start()
    {
        penguin = GameObject.Find("Penguin");
        human = GameObject.Find("Human");
        humanai = human.GetComponent<HumanAI>();
        snowballposition = GameObject.Find("initialSnowBallPosition");
    }

    private void FixedUpdate()
    {
        if (start && humanai._isThrowSnowBall)
        {
            
            Vector3 inititalSnowBallPosition = snowballposition.transform.position;
            Vector3 penguinPosition = penguin.transform.position;
            Debug.LogError(penguinPosition);
            direction = penguinPosition - inititalSnowBallPosition;
            direction = direction.normalized;
            //direction.y += 0.3f;
            rb.AddForce(direction * acceleration, ForceMode.Impulse);
            start = false;
        }
        else if (start && !humanai._isThrowSnowBall)
        {
            Debug.LogError("Снежок не летит в пингвина!");
            direction = Vector3.down;
            acceleration = 5;

            direction = direction + penguin.transform.right;
            rb.AddForce(direction.normalized * acceleration, ForceMode.Impulse);
            start = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(destroySnawBall());
    }
    IEnumerator destroySnawBall()
    {
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }
}
