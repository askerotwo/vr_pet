﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForcedThrowingWithMouseKey : MonoBehaviour
{
    private float acceleration = 30.0f;
    private GameObject rightHand;

    private bool isActive = false;

    [SerializeField] private GameObject snowBallPrefab;
    private GameObject newSnowBall;

    public void activateSnowBall()
    {
        rightHand = GameObject.Find("PlayerVR/TrackedAlias/Aliases/RightControllerAlias");
        isActive = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && isActive)
        {
            StartCoroutine("throwSnowBallCoroutine");
        }
    }

    public void StartDestroy()
    {
        StartCoroutine(destroy());
    }

    private IEnumerator destroy()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }

    private IEnumerator throwSnowBallCoroutine()
    {
        yield return new WaitForSeconds(0.2f);
        throwSnowBall();
    }

    private void throwSnowBall()
    {
        Rigidbody rigidBody = this.gameObject.GetComponent<Rigidbody>();
        rigidBody.isKinematic = false;
        rigidBody.AddForce(rightHand.transform.forward * acceleration, ForceMode.Impulse);
        isActive = false;
    }
}
