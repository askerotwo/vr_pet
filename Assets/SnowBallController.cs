﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowBallController : MonoBehaviour
{
    private Vector3 direction;
    public float acceleration;
    public Rigidbody rb;
    private bool start = true;

    private GameObject penguin;
    private GameObject human;
    penguinAI _penguinAI;

    private void Start()
    {
        penguin = GameObject.Find("Penguin");
        _penguinAI = penguin.GetComponent<penguinAI>();
        human = GameObject.Find("Human");
    }

    private void FixedUpdate()
    {


        if (start && _penguinAI.throwToPlayer)
        {
            Vector3 playerPosition;
            if (human)
            {
                playerPosition = human.transform.position;
            }
            else
            {
                playerPosition = GameObject.Find("VRPlayerPosition").transform.position;
            }

            Vector3 penguinPosition = GameObject.Find("RightArm").transform.position;
            penguinPosition += penguin.transform.right;
            direction = playerPosition - penguinPosition;
            direction = direction.normalized;
            direction.y += 0.3f;
            rb.AddForce(direction * acceleration, ForceMode.Impulse);
            start = false;
        }
        else if (start && !_penguinAI.throwToPlayer)
        {
            direction = Vector3.down;
            acceleration = 5;

            direction = direction + penguin.transform.right;
            rb.AddForce(direction.normalized * acceleration, ForceMode.Impulse);
            start = false;
        }


    }

    private void OnTriggerEnter(Collider other)
    {

        //Debug.Log("destroyed");
        StartCoroutine(destroySnawBall());

    }
    IEnumerator destroySnawBall()
    {
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }
}
