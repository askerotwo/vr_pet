﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using Zinnia.Tracking.Velocity;

public class touchDetecter : MonoBehaviour
{
    [SerializeField] private GameObject leftController;
    [SerializeField] private GameObject rightController;

    [SerializeField] private GameObject penguin;

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Interactor")
        {
            //if (leftController.GetComponent<VelocityTracker>().GetVelocity().magnitude > 4)
            ////if (Input.GetMouseButton(0))
            // {
            //    if (!penguin.GetComponent<penguinAI>().Stop)
            //    {
            //        //penguin.GetComponent<penguinAI>().sad();
            //        string action = penguin.GetComponent<penguinModel>().launchNewScene("Human hits penguin");
            //        penguin.GetComponent<penguinAI>().PenguinActions(action);

            //    }
            //}
            // else
            penguin.GetComponent<penguinAI>().stopAllIndependentActions();
            if (!penguin.GetComponent<penguinAI>().stopContinuity)
            {
                string action = penguin.GetComponent<penguinModel>().launchNewScene("Human strokes penguin");
                penguin.GetComponent<penguinAI>().PenguinActions(action);
            }
        }

        if(other.tag == "Torch")
        {
            penguin.GetComponent<penguinAI>().stopAllIndependentActions();
            if (!penguin.GetComponent<penguinAI>().stopContinuity)
            {
                string action = penguin.GetComponent<penguinModel>().launchNewScene("Human hits penguin");
                penguin.GetComponent<penguinAI>().PenguinActions(action);
            }
        }
    }
}
