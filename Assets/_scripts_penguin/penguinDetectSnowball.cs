﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class penguinDetectSnowball : MonoBehaviour
{
    [SerializeField] GameObject penguin;
    penguinAI _penguinAI;
    private penguinModel penguinmodel;
    private void Start()
    {
        _penguinAI = penguin.GetComponent<penguinAI>();
        penguinmodel = penguin.GetComponent<penguinModel>();
    }
    private void OnTriggerEnter(Collider other)
    {
        
        if(other.tag == "PlayerSnowball")
        {
            Debug.Log("snowball!!");
            penguin.GetComponent<penguinAI>().stopAllIndependentActions();
            if (!penguin.GetComponent<penguinAI>().stopContinuity)
            {
                string action = penguinmodel.launchNewScene("Human throws ball into penguin");
                _penguinAI.PenguinActions(action);
            }
        }
        //else if(other.tag == "PlayerFish")
        //{
        //    penguin.GetComponent<penguinAI>().stopAllIndependentActions();
        //    if (!penguin.GetComponent<penguinAI>().stopContinuity)
        //    {
        //        _penguinAI.waitingForFish = false;
        //        string action = penguinmodel.launchNewScene("Human feeds penguin");
        //        Debug.Log(action);
        //        _penguinAI.PenguinActions(action);
        //    }
        //}
        
    }
}
