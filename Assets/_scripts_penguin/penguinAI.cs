﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Experimental.PlayerLoop;

public class penguinAI : MonoBehaviour
{
    public static bool LOGGING_ALL_DATA = true;
    private NavMeshAgent agent;
    [SerializeField] private GameObject snowBall;
    private GameObject _snowball;
    [SerializeField] private GameObject fish;
    [SerializeField] private GameObject fishEating;
    private GameObject _fish;
    private GameObject _fishEating;
    [SerializeField] private GameObject fishPosition;
    public static int session = 1;
    [SerializeField] public GameObject headOfPenguin;
    [SerializeField] public GameObject neckOfPenguin;
    
    [SerializeField] public List<Camera> cameras;
    private Camera activecamera;

    [SerializeField] public GameObject HeadSetAlias;
    private Vector3 centerofcamera;
    private Ray ray;
    private RaycastHit raycasthit;
    private LoggingFile loggingFile;
    private Animator animator;
    private AudioSource playervraudiosource;
    private penguinModel penguinmodel;

    private int _layermask = -1;
    public float _maxDistance = 12;
    
    private Vector3 _origin;
    
    private bool isCoroutineExecuting = false;
    private bool isCoroutineExecutingSleep = false;
    private bool isCoroutineExecutingHungry = false;
    private bool isCoroutineExecutingStayingInFrontOfPlayer = false;
    private bool isCoroutineExecutingPlayful = false;
    public bool isSleep = false;
    public bool Stop = false;
    private bool isHungry = false;
    private bool isPlayful = false;
    private bool isTurning = false;
    private bool isStayingInFrontOfPlayer = false;
    private bool isAttack = false;
    public bool waitingForFish = false;
    private bool isWalking = false;
    public bool throwToPlayer = false;
    private bool eatFish = false;
    private Vector3 _lastDestination;
    public bool isTurningHead = false;
    private bool rotateReverse = false;
    private Vector3 penguinHeadForward;
    private bool stopLook = false;
    public bool stopContinuity = false;

    private float _stayTime = 7.0f;
    private float lookTime = 0.0f;

    [SerializeField] private GameObject penguinHome;
    [SerializeField] public GameObject Player;
    [SerializeField] private GameObject BasketFishPosition;
    [SerializeField] private GameObject BasketFish;
    [SerializeField] private GameObject PlayerPosition;
    [SerializeField] private GameObject BasketSnowBallPosition;
    [SerializeField] private GameObject BasketSnowBall;
    [SerializeField] private Camera humanCamera;
     
    [SerializeField] private AudioClip clipPenguinSounds;
    [SerializeField] private AudioClip clipHey;
    [SerializeField] private GameObject PlayerVR;



    public int sleepingTime = 10;

    public GameObject human;
    public System.Diagnostics.Stopwatch stopWatch;
    public System.Diagnostics.Stopwatch gameTimer;
    public Actions _lastAction;
    private float distHungry;
    private float distSleep;
    private float distStayInFrontOfPlayer;
    private float distPlayful;
    private float distWalking;
    private float distAttack;

    private bool humanisactive = false;

    private float timer;
    private Transform target = null;
    public float turnSpeed = 2.0f;
    public enum Actions
    {
        Move,
        Stay, 
        Sleep,
        Hitted,
        Waving,
        TurningFromThePlayer,
        HittedFish,
        Eating,
        WaitingForFish,
        Happy,
        noAction
    }
    void Start()
    {
        human = GameObject.Find("Human");
        if (human)
        {
            if (human.activeSelf)
            {
                humanisactive = true;
                PlayerPosition = GameObject.Find("humanPosition");
                Player = GameObject.Find("Human");
                activecamera = humanCamera;
                Debug.LogError("human was found");
            }
            else
            {
                Debug.LogError("Human wasn't found in the scene");
            }
        }

        foreach(Camera cam in cameras)
        {
            if (cam.isActiveAndEnabled)
            {
                activecamera = cam;
                break;
            }

        }


        target = Player.transform;
        animator = GetComponent<Animator>();
        penguinmodel = GetComponent<penguinModel>();
        loggingFile = GetComponent<LoggingFile>();
        
        playervraudiosource = PlayerVR.GetComponent<AudioSource>();
        stopWatch = new System.Diagnostics.Stopwatch();
        gameTimer = new System.Diagnostics.Stopwatch();
        stopWatch.Start();
        gameTimer.Start();
        centerofcamera = new Vector3(activecamera.pixelWidth / 2, activecamera.pixelHeight / 2, 0);
        penguinmodel.setupActs();
        agent = GetComponent<NavMeshAgent>();
        agent.autoRepath = true;
        _origin = transform.position;
        //headOfPenguin = GameObject.Find("LeftUpLeg");
    }

    void Update()
    {

        ray = activecamera.ScreenPointToRay(centerofcamera);
        if(Physics.Raycast(ray, out raycasthit))
        {
            if(raycasthit.transform.name == "Penguin" && !isSleep  && Vector3.Angle(headOfPenguin.transform.forward, target.position - headOfPenguin.transform.position) < 90.0f)
            {
            //Debug.Log("Angle = " + Vector3.Angle(headOfPenguin.transform.forward, target.position - headOfPenguin.transform.position));
                if (!isTurningHead)
                {
                    string action = penguinmodel.launchNewScene("Human looks at penguin");
                    PenguinActions(action);
                }
            }
        }
        
        if (stopWatch.Elapsed.Seconds >= 4)
        {
            loggingFile.UpdateCheckPoints();
            stopWatch.Restart();
        }
        // Пингвин сжимается (реакция на удар)
        if (Input.GetKeyDown(KeyCode.X) && !isSleep && !Stop)
        {

            //loggingFile.UpdateLogs("Penguin", "Human", loggingFile.ActsNumbers["Пингвин идет к человеку"], "0");
            Stop = true;
            StartCoroutine(penguinShrink());
        }


        // Пингвин подходит к человеку и клюет его (реакция на удар)
        if (Input.GetKeyDown(KeyCode.T) && !isSleep && !Stop)
        {

            //loggingFile.UpdateLogs("Penguin", "Human", loggingFile.ActsNumbers["Пингвин идет к человеку"], "0");
            Stop = true;
            isCoroutineExecuting = false;

            if (Vector3.Distance(transform.position, PlayerPosition.transform.position) < 1.2f)
            {
                agent.SetDestination(transform.position);
                StartCoroutine(penguinAttack());
            }
            else
            {
                agent.SetDestination(transform.position);
                isAttack = true;
                Move(PlayerPosition.transform.position);
            }
            loggingFile.UpdateLogs("Human", "Penguin", "Человек ударил пингвина", "0");
            loggingFile.UpdateLogs("Penguin", "Human", "Пингвин атакует человека", "1");
            //
        }

        // Пингвин поворачивает голову к человеку при взгляде на него

        //if (Input.GetKeyDown(KeyCode.T) && !isSleep && !Stop)
        //{
        //    if(isTurningHead)
        //    {
        //        isTurningHead = false;
        //        return;
        //    }
        //    isTurningHead = true;
        //    Stop = true;

        //}

        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    throwToPlayer = true;
        //    hitReaction();
        //}


        //Игрок говорит "hey"
        if (Input.GetKeyDown(KeyCode.F))
        {
            //stopAllIndependentActions();
            //playervraudiosource.PlayOneShot(clipHey);
            //loggingFile.UpdateLogs("Human", "Penguin", "Человек приветствует пингвина", "0");
            //if (!stopContinuity)
            //{
            //    string action = penguinmodel.launchNewScene("Human greetings penguin");
            //    PenguinActions(action);
            //}
        }


        if (Input.GetKeyDown(KeyCode.N) && !isSleep && !Stop)
        {
            loggingFile.UpdateLogs("Human", "Penguin", "Человек приветствует пингвина", "0");
            playervraudiosource.PlayOneShot(clipHey);
            Debug.Log("Hello");


            string action = penguinmodel.launchNewScene("HelloPinguin");
            PenguinActions(action + " HelloPinguin");
        }
            //Поворачивается к игроку и машет ему 
            if (Input.GetKeyDown(KeyCode.K) && !isSleep && !Stop)
        {
            Stop = true;
            playervraudiosource.PlayOneShot(clipHey);
            Debug.Log("Hello");
            isCoroutineExecuting = false;
            agent.SetDestination(transform.position);
            animator.SetBool("isWalking", false);
            animator.SetBool("isStaying", true);
            _lastAction = Actions.Waving;
            isTurning = true;
        }
        if (Input.GetKeyDown(KeyCode.O) && !isSleep && !Stop)
        {
            Stop = true;
            isCoroutineExecuting = false;
            agent.SetDestination(transform.position);
            isStayingInFrontOfPlayer = true;
            Move(PlayerPosition.transform.position);
        }
        //Подходит к корзине с рыбой и смотрит в нее
        if (Input.GetKeyDown(KeyCode.H) && !isSleep && !Stop)
        {
            Stop = true;
            Debug.Log("Hungry");
            isCoroutineExecuting = false;
            Move(BasketFishPosition.transform.position);
            isHungry = true;

        }
        //Подходит к корзине с снежками и смотрит в нее
        if (Input.GetKeyDown(KeyCode.G) && !isSleep && !Stop)
        {
            Stop = true;
            Debug.Log("Playful");
            isCoroutineExecuting = false;
            Move(BasketSnowBallPosition.transform.position);
            isPlayful = true;


        }
        //if (Input.GetKeyDown(KeyCode.U) && !isSleep && !Stop)
        //{
        //    Stop = true;
        //    isCoroutineExecuting = false;
        //    Debug.Log("Eating");
        //    agent.SetDestination(transform.position);
        //    isCoroutineExecuting = false;
        //    _lastAction = Actions.Eating;
        //    StartCoroutine(Eating());

        //}
        //Отворачивается от игрока
        if (Input.GetKeyDown(KeyCode.L) && !isSleep && !Stop)
        {
            Stop = true;
            playervraudiosource.PlayOneShot(clipHey);
            Debug.Log("Turn from the player");
            isCoroutineExecuting = false;
            agent.SetDestination(transform.position);
            animator.SetBool("isWalking", false);
            animator.SetBool("isStaying", true);
            _lastAction = Actions.TurningFromThePlayer;
            isTurning = true;         
        }
        ////Ждет рыбу
        //if (Input.GetKeyDown(KeyCode.Y) && !isSleep && !Stop)
        //{
        //    Stop = true;
        //    isCoroutineExecuting = false;
        //    agent.SetDestination(transform.position);
        //    animator.SetBool("isWalking", false);
        //    animator.SetBool("isStaying", true);
        //    _lastAction = Actions.WaitingForFish;
        //    isTurning = true;
        //}

        distPlayful = (agent.pathPending && isPlayful)
            ? Vector3.Distance(transform.position, BasketSnowBallPosition.transform.position)
            : agent.remainingDistance;
        distStayInFrontOfPlayer = (agent.pathPending && isStayingInFrontOfPlayer)
            ? Vector3.Distance(transform.position, PlayerPosition.transform.position)
            : agent.remainingDistance;
        distHungry = (agent.pathPending && isHungry)
            ? Vector3.Distance(transform.position, BasketFishPosition.transform.position)
            : agent.remainingDistance;
        distSleep = (agent.pathPending && isSleep)
            ? Vector3.Distance(transform.position, penguinHome.transform.position)
            : agent.remainingDistance;
        distWalking = (agent.pathPending && isWalking)
            ? Vector3.Distance(transform.position, _lastDestination)
            : agent.remainingDistance;
        distAttack = (agent.pathPending && isAttack)
            ? Vector3.Distance(transform.position, PlayerPosition.transform.position)
            : agent.remainingDistance;
        if(isAttack)
        {
           // Debug.Log("distattack = " + distAttack);
        }

        //Debug.Log(distAttack);
        if (distPlayful ==0 && !isCoroutineExecutingPlayful && isPlayful)
        {
            StartCoroutine(Playfullying());
        }
        if (isTurning && !isSleep)
        {
            Rotate();
        }
        if(isTurningHead)
        {

            RotateHead();

        }

        if(distHungry == 0  && !isCoroutineExecutingHungry && isHungry)
        {
            StartCoroutine(Hungrying());   
        }
        if (distSleep == 0  && !isCoroutineExecutingSleep && isSleep)
        {
            StartCoroutine(Sleeping());
        }
        if (distStayInFrontOfPlayer == 0  && !isCoroutineExecutingStayingInFrontOfPlayer  &&  isStayingInFrontOfPlayer)
        {
                StartCoroutine(StayingInFrontOfPlayer());
        }
        if (distAttack <= 1.2f  && isAttack)
        {
            isAttack = false;
            StartCoroutine(penguinAttack());
        }
        if (isAttack)
        {
            Move(PlayerPosition.transform.position);
        }

        // Проверяется , не в пути ли пингвин , либо он еще не достоял свое. 
        if (!isCoroutineExecuting && distWalking < 0.1 && !isSleep && !isHungry && !Stop)
        {
            //Choose the action (Penguin). 
            int rand = UnityEngine.Random.Range(0, 22);
            if (rand > 0 && rand < 14)
            {
                Debug.Log("Move");
                Move();
            }
            else if (rand > 14 && rand < 22)
            {
                Debug.Log("Stay");
                StartCoroutine(Stay());
            }
            else if (rand > 20 && rand < 22)
            {
                Sleep(new object());
            }
        }

    }

    private void FixedUpdate()
    {
        //Debug.Log(animator.GetBool(1));
      

    }



    public void sad() {
            Stop = true;
            Debug.Log("Sad penguin");
            isCoroutineExecuting = false;
            agent.SetDestination(transform.position);
            animator.SetBool("isWalking", false);
            animator.SetBool("isStaying", true);
            _lastAction = Actions.TurningFromThePlayer;
            isTurning = true;
    }
    public void happy() {
        Stop = true;
        isCoroutineExecuting = false;
        agent.SetDestination(transform.position);
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        _lastAction = Actions.Happy;
        isTurning = true;
    }

    IEnumerator HappyPenguin() {
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("isStaying", false);
        animator.SetBool("isWaitingForFish", true);
        GetComponent<AudioSource>().PlayOneShot(clipPenguinSounds);
        Debug.Log("Happy penguin");
        yield return new WaitForSeconds(2f);
        GetComponent<AudioSource>().Stop();
        animator.SetBool("isWaitingForFish", false);
        animator.SetBool("isStaying", true);
        Stop = false;
    }

    IEnumerator penguinShrink()
    {
        
        Debug.Log("penguin shrink");
        agent.SetDestination(transform.position);
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        //animator.SetBool("isShrink", true);
        //GetComponent<AudioSource>().PlayOneShot(clipPenguinSounds);
        yield return new WaitForEndOfFrame();
        animator.SetBool("isStaying", false);
        animator.SetBool("isShrink", true);
        yield return new WaitForSeconds(2f);
        //GetComponent<AudioSource>().Stop();
        animator.SetBool("isShrink", false);
        //agent.isStopped = false;
        animator.SetBool("isStaying", true);
        Stop = false;
        stopLook = false;
    }

    IEnumerator penguinAttack()
    {
        Debug.Log("IENUMARATOR");
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        yield return new WaitForSeconds(0.1f);
        animator.SetBool("isStaying", false);
        if (humanisactive)
        {
            transform.LookAt(Player.transform);
        }
        else
        {
            transform.LookAt(HeadSetAlias.transform);
        }
        GetComponent<AudioSource>().PlayOneShot(clipPenguinSounds);
        animator.SetBool("isAttack", true);
        yield return new WaitForSeconds(1.5f);
        GetComponent<AudioSource>().Stop();
        animator.SetBool("isAttack", false);
        animator.SetBool("isStaying", true);
        Stop = false;
    }
    public void Rotate()
    {
        // Determine which direction to rotate towards
        Vector3 targetDirection = target.position - transform.position;

        if (_lastAction == Actions.TurningFromThePlayer)
            targetDirection = -targetDirection;

        targetDirection.y = 0;
        // The step size is equal to speed times frame time.
        float singleStep = turnSpeed * Time.deltaTime;

        // Rotate the forward vector towards the target direction by one step
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);

        if (Vector3.Angle(transform.forward, targetDirection) < 6)
        {
            isTurning = false;
            if (_lastAction == Actions.Waving)
            {
                StartCoroutine(Waving());
            }
            else if (_lastAction == Actions.Hitted)
            {
                StartCoroutine(hitReactionCoroutine());
            }
            else if (_lastAction == Actions.TurningFromThePlayer)
            {
                StartCoroutine(TurnFromThePLayer());
            }
            else if (_lastAction == Actions.HittedFish)
            {
                StartCoroutine(hitReactionFishCoroutine());
            }
            else if(_lastAction == Actions.WaitingForFish)
            {
                StartCoroutine(WaitingForFish());
            }
            else if(_lastAction == Actions.Happy)
            {
                StartCoroutine(HappyPenguin());
            }

        }
        // Calculate a rotation a step closer to the target and applies rotation to this object
        transform.rotation = Quaternion.LookRotation(newDirection);
    }


    public void RotateHead()
    {
        if(!rotateReverse && Vector3.Angle(neckOfPenguin.transform.forward, target.position - headOfPenguin.transform.position) < 90.0f && !stopLook)
        {
            // Determine which direction to rotate towards
            Vector3 targetDirection = target.position - headOfPenguin.transform.position;



            //if (_lastAction == Actions.TurningFromThePlayer)
            //    targetDirection = -targetDirection;

            targetDirection.y = 0;
            // The step size is equal to speed times frame time.
            float singleStep = turnSpeed * 2 * Time.deltaTime;

            // Rotate the forward vector towards the target direction by one step
            Vector3 newDirection = Vector3.RotateTowards(headOfPenguin.transform.forward, targetDirection, singleStep, 0.0f);

            // Calculate a rotation a step closer to the target and applies rotation to this object

            headOfPenguin.transform.rotation = Quaternion.LookRotation(newDirection);
            if (Vector3.Angle(headOfPenguin.transform.forward, targetDirection) < 6)
            {
                rotateReverse = true;
            }
        }
        else
        {
            if(lookTime > 0.8f)
            {
                
                ///Debug.Log("povorot");
                // Determine which direction to rotate towards
                Vector3 targetDirection = neckOfPenguin.transform.forward;



                //if (_lastAction == Actions.TurningFromThePlayer)
                //    targetDirection = -targetDirection;

                // The step size is equal to speed times frame time.
                float singleStep = turnSpeed * 2 * Time.deltaTime;

                // Rotate the forward vector towards the target direction by one step
                Vector3 newDirection = Vector3.RotateTowards(headOfPenguin.transform.forward, targetDirection, singleStep, 0.0f);


                // Calculate a rotation a step closer to the target and applies rotation to this object
                //Debug.Log("headOfPenguin.transform.forward " + headOfPenguin.transform.forward);
                //Debug.Log("targetDirection " + targetDirection);

                headOfPenguin.transform.rotation = Quaternion.LookRotation(newDirection);
                //headOfPenguin.transform.rotation = new Quaternion(0, 0, 0, 0);
                //Debug.Log("hehe");
                if (Vector3.Angle(headOfPenguin.transform.forward, targetDirection) < 2.0f)
                {
                    isTurningHead = false;
                    rotateReverse = false;
                    lookTime = 0.0f;
                    //Stop = false;

                }
            }
            else
            {
                lookTime += Time.deltaTime;
            }
 
        }

        
    }
    IEnumerator WaitForNextFrame()
    {
        yield return new WaitForEndOfFrame();
    }

    IEnumerator Eating()
    {
        stopContinuity = true;
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        yield return new WaitForSeconds(0.2f);
        animator.SetBool("isStaying", false);
        animator.SetBool("isEating", true);
        yield return new WaitForSeconds(0.3f);
        _fishEating =  Instantiate(fishEating) as GameObject;
        _fishEating.transform.position = fishPosition.transform.position;
        _fishEating.transform.eulerAngles = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(1f);
        animator.SetBool("isEating", false);
        animator.SetBool("isStaying", true);
        Destroy(_fishEating.gameObject);
        stopContinuity = false;
        Stop = false;

    }
    IEnumerator StayingInFrontOfPlayer()
    {
        if (isCoroutineExecutingStayingInFrontOfPlayer)
            yield break;
        isCoroutineExecutingStayingInFrontOfPlayer = true;
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        transform.LookAt(HeadSetAlias.transform);
        yield return new WaitForSeconds(3);
        isCoroutineExecutingStayingInFrontOfPlayer = false;
        isStayingInFrontOfPlayer = false;
        penguinmodel.penguinMakeAct("GoCommunicatePenguin");
        loggingFile.UpdateLogs("Penguin", "Human", "Пингвин идет к человеку", "0");
        Stop = false;
    }

    public void hitReactionFish()
    {

        if (!eatFish)
        {
            Stop = true;
            isCoroutineExecuting = false;
            agent.SetDestination(transform.position);
            animator.SetBool("isWalking", false);
            animator.SetBool("isStaying", true);
            _lastAction = Actions.HittedFish;
            isTurning = true;
        }
        else if (eatFish)
        {
            Stop = true;
            isCoroutineExecuting = false;
            //Debug.Log("Eating");
            agent.SetDestination(transform.position);
            isCoroutineExecuting = false;
            _lastAction = Actions.Eating;
            StartCoroutine(Eating());

        }
    }
    public void hitReaction()
    {
        Stop = true;
        isCoroutineExecuting = false;
        agent.SetDestination(transform.position);
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        _lastAction = Actions.Hitted;
        isTurning = true;
    }


    IEnumerator hitReactionFishCoroutine()
    {
        animator.SetBool("isThrowingAside", true);
        animator.SetBool("isStaying", false);
        _fish = Instantiate(fish) as GameObject;
        Vector3 fishPosition = GameObject.Find("RightForeArm_end").transform.position;
        _fish.transform.position = fishPosition;
        _fish.transform.Rotate(40, -90, 0);
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("isThrowingAside", false);
        animator.SetBool("isStaying", true);
        yield return new WaitForEndOfFrame();
        Debug.Log("Turn from the player");
        isCoroutineExecuting = false;
        _lastAction = Actions.TurningFromThePlayer;
        isTurning = true;
    }
    IEnumerator hitReactionCoroutine()
    {

        if(throwToPlayer)
        {
            animator.SetBool("isThrowingAtThePlayer", true);
            animator.SetBool("isStaying", false);
            _snowball = Instantiate(snowBall) as GameObject;
            Vector3 snowBallPosition = GameObject.Find("RightArm").transform.position;
            snowBallPosition += GameObject.Find("Penguin").transform.right * 0.5f;
            //snowBallPosition += GameObject.Find("Penguin").transform.right * 0.5f;
            //_snowball.transform.Rotate(30, -90, 0);
            _snowball.transform.position = snowBallPosition;
            _snowball.transform.rotation = transform.rotation;
            yield return new WaitForSeconds(0.5f);
            animator.SetBool("isThrowingAtThePlayer", false);
            animator.SetBool("isStaying", true);
            yield return new WaitForEndOfFrame();
            Stop = false;
        }
        else if(!throwToPlayer)
        {
            animator.SetBool("isThrowingAside", true);
            animator.SetBool("isStaying", false);
            yield return new WaitForSeconds(0.2f);
            _snowball = Instantiate(snowBall) as GameObject;
            Vector3 snowBallPosition = GameObject.Find("RightForeArm_end").transform.position;
            _snowball.transform.position = snowBallPosition;
            //_snowball.transform.Rotate(40, -90, 0);
            animator.SetBool("isThrowingAside", false);
            animator.SetBool("isStaying", true);
            yield return new WaitForEndOfFrame();
            Debug.Log("Turn from the player");
            isCoroutineExecuting = false;
            _lastAction = Actions.TurningFromThePlayer;
            isTurning = true;
        }

    }

    public void WaitForFish()
    {
        Stop = true;
        isCoroutineExecuting = false;
        agent.SetDestination(transform.position);
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        _lastAction = Actions.WaitingForFish;
        isTurning = true;
        
    }

    IEnumerator reactionToFish()
    {
        yield return new WaitForSeconds(3);
        Debug.Log("reactionToFish() " + waitingForFish);
        if (waitingForFish)
        {
            Debug.Log("Ne dojdalsya ryby");
        }
        else
        {
            Debug.Log("Polychil ryby");
        }
    }
    IEnumerator Waving()
    {
        animator.SetBool("isStaying", false);
        animator.SetBool("isWaving", true);
        yield return new WaitForSeconds(1);
        animator.SetBool("isWaving", false);
        animator.SetBool("isStaying", true);
        StartCoroutine(StayCurrentTime());
        yield return new WaitForSeconds(1.5f);
        //yield return new WaitForEndOfFrame();
        Stop = false;
    }
    IEnumerator TurnFromThePLayer()
    {
        Vector3 position = transform.position;
        agent.speed = 6;
        agent.acceleration = 3;
        Move(transform.position + 4*transform.forward);
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(1);
        agent.acceleration = 1.5f;
        agent.speed = 2;
        Stop = false;
    }

    IEnumerator WaitingForFish()
    {
        StartCoroutine(reactionToFish());
        animator.SetBool("isStaying", false);
        animator.SetBool("isWaitingForFish", true);
        GetComponent<AudioSource>().PlayOneShot(clipPenguinSounds);
        yield return new WaitForSeconds(4f);
        GetComponent<AudioSource>().Stop();
        animator.SetBool("isWaitingForFish", false);
        animator.SetBool("isStaying", true);
        Stop = false;
    }



    public void Move()
    {
        _lastAction = Actions.Move;
        animator.SetBool("isStaying", false);
        animator.SetBool("isWalking", true);
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * _maxDistance;
        randomDirection += _origin;
        NavMeshHit navHit;
        NavMesh.SamplePosition(randomDirection, out navHit, _maxDistance, _layermask);
        _lastDestination = navHit.position;
        isWalking = true;
        agent.SetDestination(navHit.position);
    }
    IEnumerator Hungrying()
    {
        if (isCoroutineExecutingHungry)
            yield break;
        isCoroutineExecutingHungry = true;
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        transform.LookAt(BasketFish.transform);
        animator.SetBool("isStaying", false);
        animator.SetBool("isLowerTheHead", true);
        GetComponent<AudioSource>().PlayOneShot(clipPenguinSounds);
        yield return new WaitForSeconds(4);
        GetComponent<AudioSource>().Stop();
        isHungry = false;
        isCoroutineExecutingHungry = false;
        animator.SetBool("isLowerTheHead", false);
        animator.SetBool("isStaying", true);
        penguinmodel.penguinMakeAct("GoEatPenguin");
        loggingFile.UpdateLogs("Penguin", "No target", "Пингвин идет к корзине с рыбой", "0");
        Stop = false;
    }

    IEnumerator Playfullying()
    {
        stopContinuity = true;
        if (isCoroutineExecutingPlayful)
            yield break;
        isCoroutineExecutingPlayful = true;
        Debug.Log("Move to origin1");
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        transform.LookAt(BasketSnowBall.transform);
        animator.SetBool("isStaying", false);
        animator.SetBool("isLowerTheHead", true);
        GetComponent<AudioSource>().PlayOneShot(clipPenguinSounds);
        yield return new WaitForSeconds(4);
        GetComponent<AudioSource>().Stop();
        isPlayful = false;
        isCoroutineExecutingPlayful = false;
        animator.SetBool("isLowerTheHead", false);
        animator.SetBool("isStaying", true);
        //yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(0.01f);
        Move(_origin);
        penguinmodel.penguinMakeAct("GoPlayPenguin");
        loggingFile.UpdateLogs("Penguin", "No target", "Пингвин идет к корзине со снежками", "0");
        stopContinuity = false;
        Stop = false;
    }
    public void Move(Vector3 pos)
    {
        animator.SetBool("isStaying", false);
        animator.SetBool("isWalking", true);
        NavMeshHit navHit;
        NavMesh.SamplePosition(pos, out navHit, _maxDistance, _layermask);
        _lastDestination = navHit.position;
        isWalking = true;
        agent.SetDestination(navHit.position);
    }

    public void penguinstayforhuman()
    {
        //_lastAction = Actions.Stay;
        //animator.SetBool("isWalking", false);
        //animator.SetBool("isStaying", true);
        //yield return new WaitForSeconds(_stayTime);
        //isCoroutineExecuting = false;
        //animator.SetBool("isStaying", false);
        StartCoroutine(Stay());
    }

    IEnumerator Stay()
    {
        Debug.LogError("Penguin is staying");
        _lastAction = Actions.Stay;
        if (isCoroutineExecuting)
            yield break;
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        isCoroutineExecuting = true;
        //Время , которое пингвин стоит на месте. 
        yield return new WaitForSeconds(_stayTime);
        isCoroutineExecuting = false;
        animator.SetBool("isStaying", false);
    }

    IEnumerator StayCurrentTime()
    {
        _lastAction = Actions.Stay;
        if (isCoroutineExecuting)
            yield break;
        animator.SetBool("isWalking", false);
        animator.SetBool("isStaying", true);
        isCoroutineExecuting = true;
        //Время , которое пингвин стоит на месте. 
        yield return new WaitForSeconds(1.5f);
        isCoroutineExecuting = false;
        animator.SetBool("isStaying", false);
    }



    public void Sleep(object num)
    {
        Stop = true;
        _lastAction = Actions.Sleep;
        Move(penguinHome.transform.position);
        isCoroutineExecuting = false;
        isSleep = true;
        
    }

    IEnumerator Hungry()
    {
        Move(BasketFishPosition.transform.position);
        yield return new WaitForEndOfFrame();
        isCoroutineExecutingHungry = false;
        isHungry = true;
    }
    
    IEnumerator Sleeping ()
    {
        stopContinuity = true;
        if (isCoroutineExecutingSleep)
            yield break;
        animator.SetBool("isStaying", false);
        animator.SetBool("isWalking", false);
        animator.SetBool("isSleeping", true);
        isCoroutineExecutingSleep = true;
        yield return new WaitForSeconds(sleepingTime);
        isSleep = false;
        isCoroutineExecutingSleep = false;
        animator.SetBool("isSleeping", false);
        penguinmodel.penguinMakeAct("GoSleepPinguin");
        loggingFile.UpdateLogs("Penguin", "No target", "Пингвин идет спать", "0");
        Stop = false;
        stopContinuity = false;
    }

    public void GoToPlayer()
    {
        if(false)
        {
            Move(Player.GetComponent<Transform>().position);
        }
    }

    public void stopAllIndependentActions()
    {
        isHungry = false;
        isPlayful = false;
        isSleep = false;
        penguinmodel.timetoplay = false;
        penguinmodel.timetoeat = false;
        penguinmodel.TimerToEat = 0;
        penguinmodel.TimerToPlay = 0;
        _lastAction = Actions.noAction;
    }

    public void PenguinActions(String action)
    {
        if (action == "Penguin greetings human")
        {
            loggingFile.UpdateLogs("Penguin", "Human", "Пингвин приветствует человека в ответ", "1");
            Debug.Log("Пингвин приветствует человека в ответ");
            Stop = true;
            //playervraudiosource.PlayOneShot(clipHey);
            Debug.Log("Hello");
            isCoroutineExecuting = false;
            agent.SetDestination(transform.position);
            animator.SetBool("isWalking", false);
            animator.SetBool("isStaying", true);
            _lastAction = Actions.Waving;
            isTurning = true;
        }

        if (action == "Penguin ignore human greeting")
        {
            loggingFile.UpdateLogs("Penguin", "Human", "Пингвин игнорирует приветствие человека", "1");
            Debug.Log("NoHello");
        }
        if (action == "goToBoxWithFish")
        {
            if (!isSleep && !Stop)
            {
                Stop = true;
                Debug.Log("Hungry");
                isCoroutineExecuting = false;
                Move(BasketFishPosition.transform.position);
                isHungry = true;
            }
        }

        if (action == "goToBoxWithSnowBalls")
        {
            if (!isSleep && !Stop)
            {
                Stop = true;
                Debug.Log("Playful");
                isCoroutineExecuting = false;
                Move(BasketSnowBallPosition.transform.position);
                isPlayful = true;
            }
        }

        if (action == "GoCommunicatePenguin")
        {
            if (!isSleep && !Stop)
            {
                Stop = true;
                isCoroutineExecuting = false;
                agent.SetDestination(transform.position);
                isStayingInFrontOfPlayer = true;
                Move(PlayerPosition.transform.position);
            }
        }

        if (action == "turnFromPlayer")
        {
                Stop = true;
                Debug.Log("Turn from the player");
                isCoroutineExecuting = false;
                agent.SetDestination(transform.position);
                animator.SetBool("isWalking", false);
                animator.SetBool("isStaying", true);
                _lastAction = Actions.TurningFromThePlayer;
                isTurning = true;
        }

        if (action == "turnToPlayerAndMashetKrylyamijdetryby")
        {
            if (!isSleep && !Stop)
            {
                Stop = true;
                isCoroutineExecuting = false;
                agent.SetDestination(transform.position);
                animator.SetBool("isWalking", false);
                animator.SetBool("isStaying", true);
                _lastAction = Actions.WaitingForFish;
                isTurning = true;
            }
        }
        if (action == "Penguin throws ball into human")
        {
                loggingFile.UpdateLogs("Human", "Penguin", "Человек попал снежком в пингвина", "0");
                loggingFile.UpdateLogs("Penguin", "Human", "Пингвин кидает снежок обратно в человека", "1");
                throwToPlayer = true;
                hitReaction();
        }
        if (action == "Penguin throws out human ball")
        {
                loggingFile.UpdateLogs("Human", "Penguin", "Человек попал снежком в пингвина", "0");
                loggingFile.UpdateLogs("Penguin", "Human", "Пингвин выбрасывает снежок", "1");
                throwToPlayer = false;
                hitReaction();
        }
        if (action == "Penguin throws out human fish")
        {
                loggingFile.UpdateLogs("Human", "Penguin", "Человек дает пингвину рыбу", "1");
                loggingFile.UpdateLogs("Penguin", "Human", "Пингвин выбрасывает рыбу которую ему дал человек", "1");
                eatFish = false;
                hitReactionFish();
        }
        if (action == "Penguin eats human fish")
        {
                loggingFile.UpdateLogs("Human", "Penguin", "Человек дает пингвину рыбу", "1");
                loggingFile.UpdateLogs("Penguin", "Human", "Пингвин ест рыбу которую ему дал человек", "1");
                eatFish = true;
                hitReactionFish();
        }
        if (action == "Penguin loves human stroking")
        {
                loggingFile.UpdateLogs("Human", "Penguin", "Человек гладит пингвина", "0");
                loggingFile.UpdateLogs("Penguin", "Human", "Пингвину нравится поглаживание человека", "1");
                happy();
        }
        if (action == "Penguin evades human stroking")
        {
                loggingFile.UpdateLogs("Human", "Penguin", "Человек гладит пингвина", "0");
                loggingFile.UpdateLogs("Penguin", "Human", "Пингвину не нравится поглаживание человека", "1");
                PenguinActions("turnFromPlayer");
        }
        if (action == "Penguin runs away from human")
        {
            loggingFile.UpdateLogs("Human", "Penguin", "Человек ударил пингвина", "0");
            loggingFile.UpdateLogs("Penguin", "Human", "Пингвин отворачивается и уходит после удара человека", "1");
            sad();
        }
        if (action == "Penguin attacks human")
        {

            Stop = true;
            isCoroutineExecuting = false;
            if (Vector3.Distance(transform.position, PlayerPosition.transform.position) < 1.2f)
            {
                agent.SetDestination(transform.position);
                StartCoroutine(penguinAttack());
            }
            else
            {
                agent.SetDestination(transform.position);
                isAttack = true;
                Move(PlayerPosition.transform.position);
            }
            //Move(PlayerPosition.transform.position);
            loggingFile.UpdateLogs("Human", "Penguin", "Человек ударил пингвина", "0");
            loggingFile.UpdateLogs("Penguin", "Human", "Пингвин атакует человека", "1");
        }
        if (action == "Penguin shrinks")
        {
            stopLook = true;
            Stop = true;
            StartCoroutine(penguinShrink());
            loggingFile.UpdateLogs("Human", "Penguin", "Человек ударил пингвина", "0");
            loggingFile.UpdateLogs("Penguin", "Human", "Пингвин сжимается", "1");
        }
        if (action == "Penguin turns head to human")
        {
            isTurningHead = true;
            loggingFile.UpdateLogs("Human", "Penguin", "Человек посмотрел на пингвина", "0");
            loggingFile.UpdateLogs("Penguin", "Human", "Пингвин посмотрел на человека", "1");
        }
        if (action == "Penguin ignore human looking")
        {
            loggingFile.UpdateLogs("Human", "Penguin", "Человек посмотрел на пингвина", "0");
            loggingFile.UpdateLogs("Penguin", "Human", "Пингвин игнорирует взгляд человека", "1");
        }
        if(action == "goSleepPenguin")
        {
            if (!isSleep && !Stop)
            {
                isSleep = true;
                Sleep(new object());
            }
        }
    }


}
