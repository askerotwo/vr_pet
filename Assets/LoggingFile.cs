﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class LoggingFile : MonoBehaviour
{
    public static string LOGS_FOLDER = "\\PENGUIN_LOGS";
    public static string LOGS_PATH = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + LOGS_FOLDER;
    
    public static string EVENT_LOGS = "\\EventLogs";
    public static string CHECK_POINTS = "\\CheckPoints";
    public static string ALL_ACTS = "\\AllActs";

    StreamWriter eventsLogs;
    StreamWriter checkPoints;
    public Dictionary<string, string> ActsNumbers = new Dictionary<string, string>();

    private penguinModel penguinModel;
    private penguinAI penguinAI;

    void Start()
    {
        penguinModel = GetComponent<penguinModel>();
        penguinAI = GetComponent<penguinAI>();

        System.IO.Directory.CreateDirectory(LOGS_PATH);
        if (penguinAI.LOGGING_ALL_DATA == false) return;
        string writePath = LOGS_PATH;
        while (File.Exists(writePath + EVENT_LOGS + penguinAI.session.ToString() + ".csv"))
        {
            ++penguinAI.session;
        }
        writePath = LOGS_PATH + EVENT_LOGS + penguinAI.session.ToString() + ".csv";
        eventsLogs = new StreamWriter(writePath, true);
        using (eventsLogs)
            eventsLogs.WriteLine(DateTime.Now.ToString("MM.dd.yyyy hh:mm:ss.fff") + "," +
            "Time From start," +
            "Action author," +
            "Action target," +
            "Action number," +
            "Response, " +
            "Human Appraisals Valence," +
            "Human Appraisals Arousal," +
            "Human Appraisals Dominance," +
            "Human Feelings Valence," +
            "Human Feelings Arousal," +
            "Human Feelings Dominance," +
            "Penguin Appraisals Valence," +
            "Penguin Appraisals Arousal," +
            "Penguin Appraisals Dominance," +
            "Penguin Somatic Pain," +
            "Penguin Somatic Energy," +
            "Penguin Somatic Satiety," +
            "Penguin Somatic Activity," +
            "Penguin Somatic Sociability," +
            "Moral schema," +
            "Coordinate X penguin," +
            "Coordinate Y penguin," +
            "Coordinate Z penguin," +
            "Azimuth penguin," +
            "Coordinate X human," +
            "Coordinate Y human," +
            "Coordinate Z human," +
            "Azimuth human,"
            );
        writePath = LOGS_PATH + CHECK_POINTS + penguinAI.session.ToString() + ".csv";
        checkPoints = new StreamWriter(writePath, true);
        using (checkPoints)
            checkPoints.WriteLine(DateTime.Now.ToString("MM.dd.yyyy hh:mm:ss.fff") + "," +
            "Time from start," +
            "Human Appraisals Valence," +
            "Human Appraisals Arousal," +
            "Human Appraisals Dominance," +
            "Human Feelings Valence," +
            "Human Feelings Arousal," +
            "Human Feelings Dominance," +
            "Penguin Appraisals Valence," +
            "Penguin Appraisals Arousal," +
            "Penguin Appraisals Dominance," +
            "Penguin Somatic Pain," +
            "Penguin Somatic Energy," +
            "Penguin Somatic Satiety," +
            "Penguin Somatic Activity," +
            "Penguin Somatic Sociability," +
            "Moral schema," +
            "Coordinate X penguin," +
            "Coordinate Y penguin," +
            "Coordinate Z penguin," +
            "Azimuth penguin," +
            "Coordinate X human," +
            "Coordinate Y human," +
            "Coordinate Z human," +
            "Azimuth human,"
            );
    }

    public string massiveToString(double[] arr)
    {
        string retValue = "";
        for (int i = 0; i < arr.Length; ++i)
        {
            Debug.Log(arr[i]);
            retValue += Math.Round(arr[i], 2).ToString();
            if (i < arr.Length - 1)
            {
                retValue += ",";
            }
        }
        retValue += "";
        return retValue;
    }

    public string vector3ToString(Vector3 arr)
    {
        string retValue = "";
        retValue += (Math.Round(arr.x, 2)).ToString() + ",";
        retValue += (Math.Round(arr.y, 2)).ToString() + ",";
        retValue += (Math.Round(arr.z, 2)).ToString();
        return retValue;
    }

    public void UpdateCheckPoints()
    {
        if (penguinAI.LOGGING_ALL_DATA == false) return;
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        string penguinAppraisals = massiveToString(penguinModel.getPenguinAppraisals());
        string penguinBodyFactor = massiveToString(penguinModel.getPenguinBodyFactor());
        string humanAppraisals = massiveToString(penguinModel.getHumanAppraisals());
        string humanFeelings = massiveToString(penguinModel.getHumanFeelings());
        string humanCharacteristic = penguinModel.getHumanCharacteristic();
        string humanPosition = vector3ToString(penguinAI.Player.transform.position);
        string penguinPosition = vector3ToString(transform.position);
        string humanAzimuth = Math.Round(penguinAI.Player.transform.rotation.eulerAngles.y, 2).ToString();
        string penguinAzimuth = Math.Round(transform.rotation.eulerAngles.y, 2).ToString();
        string writePath = LOGS_PATH + CHECK_POINTS + penguinAI.session.ToString() + ".csv";
        checkPoints = new StreamWriter(writePath, true, Encoding.GetEncoding("windows-1251"));
        using (checkPoints)
            checkPoints.WriteLine(DateTime.Now.ToString("MM.dd.yyyy hh:mm:ss.fff") + "," +
            Math.Round(penguinAI.gameTimer.Elapsed.TotalSeconds, 3).ToString() + "," +
            humanAppraisals + "," +
            humanFeelings + "," +
            penguinAppraisals + "," +
            penguinBodyFactor + "," +
            humanCharacteristic + "," +
            penguinPosition + "," +
            penguinAzimuth + "," +
            humanPosition + "," +
            humanAzimuth
            );
    }

    public Dictionary<string, string> installVectorActs()
    {
        var map = GetComponent<ActionsOnRussian>().getDictionaryActsName();
        int count = 0;
        foreach (var el in map)
        {
            ++count;
            ActsNumbers.Add(el.Key, count.ToString());
            Debug.Log(el.Key + " " + count);

        }
        return ActsNumbers;
    }

    public Dictionary<string, string> getActionsNumber()
    {
        return ActsNumbers;
    }

    public void GettedNewAction(string author, string target, string actionNumber, string response)
    {
        UpdateLogs(author, target, actionNumber, response);
    }

    public void UpdateLogs(string author, string target, string authorAction, string Response)
    {
        if (penguinAI.LOGGING_ALL_DATA == false) return;
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        string penguinAppraisals = massiveToString(penguinModel.getPenguinAppraisals());
        string penguinBodyFactor = massiveToString(penguinModel.getPenguinBodyFactor());
        string humanAppraisals = massiveToString(penguinModel.getHumanAppraisals());
        string humanFeelings = massiveToString(penguinModel.getHumanFeelings());
        string humanCharacteristic = penguinModel.getHumanCharacteristic();
        string humanPosition = vector3ToString(penguinAI.Player.transform.position);
        string penguinPosition = vector3ToString(transform.position);
        string humanAzimuth = penguinAI.Player.transform.rotation.eulerAngles.y.ToString();
        string penguinAzimuth = transform.rotation.eulerAngles.y.ToString();
        string writePath = LOGS_PATH + EVENT_LOGS + penguinAI.session.ToString() + ".csv";
        eventsLogs = new StreamWriter(writePath, true, Encoding.GetEncoding("windows-1251"));
        using (eventsLogs)
            eventsLogs.WriteLine(DateTime.Now.ToString("MM.dd.yyyy hh:mm:ss.fff") + "," +
            Math.Round(penguinAI.gameTimer.Elapsed.TotalSeconds, 3).ToString() + "," +
            author + "," +
            target + "," +
            authorAction + "," +
            Response + "," +
            humanAppraisals + "," +
            humanFeelings + "," +
            penguinAppraisals + "," +
            penguinBodyFactor + "," +
            humanCharacteristic + "," +
            penguinPosition + "," +
            penguinAzimuth + "," +
            humanPosition + "," +
            humanAzimuth
            );
    }

    // Update is called once per frame
    void Update()
    {

    }
}