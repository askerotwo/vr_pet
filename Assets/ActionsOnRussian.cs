﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using Valve.Newtonsoft.Json;

public class ActionsOnRussian : MonoBehaviour
{
    // Start is called before the first frame update

    int session = 1;
    public static Dictionary<string, Act> allActs = new Dictionary<string, Act>();
    string writePath;
    StreamWriter AllActs;
    private LoggingFile loggingFile;

    public static Dictionary<string, string> eng2russianActs = new Dictionary<string, string>()
        {
            { "Human greetings penguin", "Человек приветствует пингвина" },
            { "Penguin greetings human", "Пингвин приветствует человека в ответ" },
            { "Penguin ignore human greeting", "Пингвин игнорирует приветствие человека" },
            { "Human feeds penguin", "Человек дает пингвину рыбу" },
            { "Penguin eats human fish", "Пингвин ест рыбу которую ему дал человек" },
            { "Penguin throws out human fish", "Пингвин выбрасывает рыбу которую ему дал человек" },
            { "Human throws ball into penguin", "Человек попал снежком в пингвина" },
            { "Penguin throws ball into human", "Пингвин кидает снежок обратно в человека" },
            { "Penguin throws out human ball", "Пингвин выбрасывает снежок" },
            { "Human strokes penguin", "Человек гладит пингвина" },
            { "Penguin loves human stroking", "Пингвину нравится поглаживание человека" },
            { "Penguin evades human stroking", "Пингвину не нравится поглаживание человека" },
            { "Human hits penguin", "Человек ударил пингвина" },
            { "Penguin runs away from human", "Пингвин отворачивается и уходит после удара человека" },
            { "Penguin attacks human", "Пингвин атакует человека" },
            { "Penguin shrinks", "Пингвин сжимается" },
            { "GoSleepPinguin", "Пингвин идет спать" },
            { "GoPlayPenguin", "Пингвин идет к корзине со снежками" },
            { "GoEatPenguin", "Пингвин идет к корзине с рыбой" },
            { "GoCommunicatePenguin", "Пингвин идет к человеку" },
            { "Human looks at penguin", "Человек посмотрел на пингвина" },
            { "Penguin turns head to human", "Пингвин посмотрел на человека" },
            { "Penguin ignore human looking", "Пингвин игнорирует взгляд человека" },
        };

    public class Act
    {
        private double[] bodyFactorForTarget;
        private double[] moralFactorForTarget;
        private double[] moralFactorForAuthor;

        public Act()
        {
            bodyFactorForTarget = new double[5];
            moralFactorForTarget = new double[3];
            moralFactorForAuthor = new double[3];
        }
        public void setBodyFactorForTarget(double[] values)
        {
            for (int i = 0; i < bodyFactorForTarget.Length; ++i)
            {
                bodyFactorForTarget[i] = values[i];
            }
        }
        public void setMoralFactorForTarget(double[] values)
        {
            for (int i = 0; i < moralFactorForTarget.Length; ++i)
            {
                moralFactorForTarget[i] = values[i];
            }
        }
        public void setMoralFactorForAuthor(double[] values)
        {
            for (int i = 0; i < moralFactorForAuthor.Length; ++i)
            {
                moralFactorForAuthor[i] = values[i];
            }
        }
        public double[] getBodyFactorForTarget()
        {
            return bodyFactorForTarget;
        }
        public double[] getMoralFactorForTarget()
        {
            return moralFactorForTarget;
        }

        public double[] getMoralFactorForAuthor()
        {
            return moralFactorForAuthor;
        }
    };
    public Dictionary<string, Act> getDictionaryActsName()
    {
        return allActs;
    }

    void Start()
    {
        loggingFile = GetComponent<LoggingFile>();

        Dictionary<string, Act> allActsTemp = new Dictionary<string, Act>();
        Dictionary<string, Act> allIndependentActionsTemp = new Dictionary<string, Act>();

        allActsTemp = JsonConvert.DeserializeObject<Dictionary<string, Act>>(File.ReadAllText(penguinModel.JSON_PATH_ALL_ACTIONS));
        allIndependentActionsTemp = JsonConvert.DeserializeObject<Dictionary<string, Act>>(File.ReadAllText(penguinModel.JSON_PATH_INDEPENDENT_PENGUIN_ACTIONS));

        foreach(var act in allActsTemp)
        {
            if (eng2russianActs.ContainsKey(act.Key))
            {
                allActs.Add(eng2russianActs[act.Key], act.Value);
            }
            
        }
        foreach (var act in allIndependentActionsTemp)
        {
            if (eng2russianActs.ContainsKey(act.Key))
            {
                allActs.Add(eng2russianActs[act.Key], act.Value);
            }
        }

        allActs.Add("Человек взял в руки рыбу", new Act());
        allActs["Человек взял в руки рыбу"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0, 0 });
        allActs["Человек взял в руки рыбу"].setMoralFactorForTarget(new double[] { 0, 0, 0 });
        allActs["Человек взял в руки рыбу"].setMoralFactorForAuthor(new double[] { 0, 0, 0 });

        allActs.Add("Пингвин просит рыбу", new Act());
        allActs["Пингвин просит рыбу"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0, 0 });
        allActs["Пингвин просит рыбу"].setMoralFactorForTarget(new double[] { 0, 0, 0 });
        allActs["Пингвин просит рыбу"].setMoralFactorForAuthor(new double[] { 0, 0, 0 });



        allActs.Add("Человек взял в руки снежок", new Act());
        allActs["Человек взял в руки снежок"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0, 0 });
        allActs["Человек взял в руки снежок"].setMoralFactorForTarget(new double[] { 0, 0, 0 });
        allActs["Человек взял в руки снежок"].setMoralFactorForAuthor(new double[] { 0, 0, 0 });

        //allActs.Add("Пингвин выбрасывает рыбу которую ему дал человек", new Act());
        //allActs["Пингвин выбрасывает рыбу которую ему дал человек"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0, -0.3 });
        //allActs["Пингвин выбрасывает рыбу которую ему дал человек"].setMoralFactorForTarget(new double[] { -0.3, -0.3, 0.15 });
        //allActs["Пингвин выбрасывает рыбу которую ему дал человек"].setMoralFactorForAuthor(new double[] { -0.4, -0.2, -0.3 });

        //allActs.Add("Пингвин ест рыбу которую ему дал человек", new Act());
        //allActs["Пингвин ест рыбу которую ему дал человек"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0.1, 0.1 });
        //allActs["Пингвин ест рыбу которую ему дал человек"].setMoralFactorForTarget(new double[] { 0.12, 0.05, 0.1 });
        //allActs["Пингвин ест рыбу которую ему дал человек"].setMoralFactorForAuthor(new double[] { 0.35, 0.15, -0.1 });


        //allActs.Add("Человек попал снежком в пингвина", new Act());
        //allActs["Человек попал снежком в пингвина"].setBodyFactorForTarget(new double[] { 0.05, -0.2, -0.1, 0.7, 0.3 });
        //allActs["Человек попал снежком в пингвина"].setMoralFactorForTarget(new double[] { 0.1, 0.6, 0.05 });
        //allActs["Человек попал снежком в пингвина"].setMoralFactorForAuthor(new double[] { 0.1, 0.6, 0.05 });

        //allActs.Add("Человек приветствует пингвина", new Act());
        //allActs["Человек приветствует пингвина"].setBodyFactorForTarget(new double[] { 0, -0.05, 0, 0.3, 0.5 });
        //allActs["Человек приветствует пингвина"].setMoralFactorForTarget(new double[] { 0.5, 0.05, -0.1 });
        //allActs["Человек приветствует пингвина"].setMoralFactorForAuthor(new double[] { 0.5, 0.05, -0.15 });

        //allActs.Add("Пингвин приветствует человека в ответ", new Act());
        //allActs["Пингвин приветствует человека в ответ"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0.1, 0.1 });
        //allActs["Пингвин приветствует человека в ответ"].setMoralFactorForTarget(new double[] { 0.12, 0.05, 0.1 });
        //allActs["Пингвин приветствует человека в ответ"].setMoralFactorForAuthor(new double[] { 0.35, 0.15, -0.1 });

        //allActs.Add("Пингвин игнорирует приветствие человека", new Act());
        //allActs["Пингвин игнорирует приветствие человека"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0, -0.3 });
        //allActs["Пингвин игнорирует приветствие человека"].setMoralFactorForTarget(new double[] { -0.3, -0.3, 0.15 });
        //allActs["Пингвин игнорирует приветствие человека"].setMoralFactorForAuthor(new double[] { -0.4, -0.2, -0.3 });

        //allActs.Add("Человек дает пингвину рыбу", new Act());
        //allActs["Человек дает пингвину рыбу"].setBodyFactorForTarget(new double[] { 0, -0.05, 0.7, -0.1, 0.2 });
        //allActs["Человек дает пингвину рыбу"].setMoralFactorForTarget(new double[] { 0.5, 0.05, -0.1 });
        //allActs["Человек дает пингвину рыбу"].setMoralFactorForAuthor(new double[] { 0.7, 0.1, 0.1 });

        //allActs.Add("Пингвин выбрасывает снежок", new Act());
        //allActs["Пингвин выбрасывает снежок"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0, -0.3 });
        //allActs["Пингвин выбрасывает снежок"].setMoralFactorForTarget(new double[] { -0.3, -0.3, 0.15 });
        //allActs["Пингвин выбрасывает снежок"].setMoralFactorForAuthor(new double[] { -0.4, -0.2, -0.3 });

        //allActs.Add("Пингвин кидает снежок обратно в человека", new Act());
        //allActs["Пингвин кидает снежок обратно в человека"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0.1, 0.1 });
        //allActs["Пингвин кидает снежок обратно в человека"].setMoralFactorForTarget(new double[] { 0.12, 0.05, 0.1 });
        //allActs["Пингвин кидает снежок обратно в человека"].setMoralFactorForAuthor(new double[] { 0.35, 0.15, -0.1 });


        //allActs.Add("Человек гладит пингвина", new Act());
        //allActs["Человек гладит пингвина"].setBodyFactorForTarget(new double[] { -0.2, 0, 0, 0.1, 0.7 });
        //allActs["Человек гладит пингвина"].setMoralFactorForTarget(new double[] { 0.7, -0.15, -0.15 });
        //allActs["Человек гладит пингвина"].setMoralFactorForAuthor(new double[] { 0.7, -0.15, 0.15 });

        //allActs.Add("Пингвину не нравится поглаживание человека", new Act());
        //allActs["Пингвину не нравится поглаживание человека"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0, -0.3 });
        //allActs["Пингвину не нравится поглаживание человека"].setMoralFactorForTarget(new double[] { -0.3, -0.3, 0.15 });
        //allActs["Пингвину не нравится поглаживание человека"].setMoralFactorForAuthor(new double[] { -0.4, -0.2, -0.3 });

        //allActs.Add("Пингвину нравится поглаживание человека", new Act());
        //allActs["Пингвину нравится поглаживание человека"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0.1, 0.1 });
        //allActs["Пингвину нравится поглаживание человека"].setMoralFactorForTarget(new double[] { 0.12, 0.05, 0.1 });
        //allActs["Пингвину нравится поглаживание человека"].setMoralFactorForAuthor(new double[] { 0.35, 0.15, -0.1 });


        //allActs.Add("Человек ударил пингвина", new Act());
        //allActs["Человек ударил пингвина"].setBodyFactorForTarget(new double[] { 0.7, -0.2, 0, 0, -0.2 });
        //allActs["Человек ударил пингвина"].setMoralFactorForTarget(new double[] { -0.7, -0.15, -0.35 });
        //allActs["Человек ударил пингвина"].setMoralFactorForAuthor(new double[] { -0.7, 0.2, 0.4 });

        //allActs.Add("Пингвин отворачивается и уходит после удара человека", new Act());
        //allActs["Пингвин отворачивается и уходит после удара человека"].setBodyFactorForTarget(new double[] { 0, 0, 0, 0, 0 });
        //allActs["Пингвин отворачивается и уходит после удара человека"].setMoralFactorForTarget(new double[] { 0, 0, 0 });
        //allActs["Пингвин отворачивается и уходит после удара человека"].setMoralFactorForAuthor(new double[] { 0, 0, 0 });


        //allActs.Add("Пингвин идет спать", new Act());
        //allActs["Пингвин идет спать"].setBodyFactorForTarget(new double[] { -0.2, 2, -0.3, -0.4, -0.1 });
        //allActs["Пингвин идет спать"].setMoralFactorForTarget(new double[] { 0.1, -0.8, 0 });
        //allActs["Пингвин идет спать"].setMoralFactorForAuthor(new double[] { 0, -0.3, 0 });

        //allActs.Add("Пингвин идет к корзине со снежками", new Act());
        //allActs["Пингвин идет к корзине со снежками"].setBodyFactorForTarget(new double[] { 0, -0.1, 0, 0.2, 0.2 });
        //allActs["Пингвин идет к корзине со снежками"].setMoralFactorForTarget(new double[] { 0.6, 0.3, -0.15 });
        //allActs["Пингвин идет к корзине со снежками"].setMoralFactorForAuthor(new double[] { 0.4, 0.4, -0.05 });

        //allActs.Add("Пингвин идет к корзине с рыбой", new Act());
        //allActs["Пингвин идет к корзине с рыбой"].setBodyFactorForTarget(new double[] { 0, -0.05, 0, 0.1, 0.15 });
        //allActs["Пингвин идет к корзине с рыбой"].setMoralFactorForTarget(new double[] { 0.1, 0.2, -0.25 });
        //allActs["Пингвин идет к корзине с рыбой"].setMoralFactorForAuthor(new double[] { 0.15, 0.1, 0.15 });

        //allActs.Add("Пингвин идет к человеку", new Act());
        //allActs["Пингвин идет к человеку"].setBodyFactorForTarget(new double[] { 0, -0.05, 0, 0.1, 0.3 });
        //allActs["Пингвин идет к человеку"].setMoralFactorForTarget(new double[] { 0.7, 0.2, 0 });
        //allActs["Пингвин идет к человеку"].setMoralFactorForAuthor(new double[] { 0.5, 0.3, 0 });

        //////

        //allActs.Add("Пингвин атакует человека", new Act());
        //allActs["Пингвин атакует человека"].setBodyFactorForTarget(new double[] { 0, -0.05, 0, 0.1, 0.3 });
        //allActs["Пингвин атакует человека"].setMoralFactorForTarget(new double[] { 0.7, 0.2, 0 });
        //allActs["Пингвин атакует человека"].setMoralFactorForAuthor(new double[] { 0.5, 0.3, 0 });

        //allActs.Add("Пингвин сжимается", new Act());
        //allActs["Пингвин сжимается"].setBodyFactorForTarget(new double[] { 0, -0.05, 0, 0.1, 0.3 });
        //allActs["Пингвин сжимается"].setMoralFactorForTarget(new double[] { 0.7, 0.2, 0 });
        //allActs["Пингвин сжимается"].setMoralFactorForAuthor(new double[] { 0.5, 0.3, 0 });

        loggingFile.installVectorActs();

        if (penguinAI.LOGGING_ALL_DATA == false) return;
        
        writePath = LoggingFile.LOGS_PATH;
        while (File.Exists(writePath + LoggingFile.ALL_ACTS + session.ToString() + ".csv"))
        {
            ++session;
        }
        writePath = LoggingFile.LOGS_PATH + LoggingFile.ALL_ACTS + session.ToString() + ".csv";
        AllActs = new StreamWriter(writePath, false, Encoding.GetEncoding("windows-1251"));
        using (AllActs)
            AllActs.WriteLine(
            "Номер действия," +
            "Название действия," +
            "Human Appraisals Valence," +
            "Human Appraisals Arousal," +
            "Human Appraisals Dominance," +
            "Penguin Appraisals Valence," +
            "Penguin Appraisals Arousal," +
            "Penguin Appraisals Dominance," +
            "Penguin Somatic Pain," +
            "Penguin Somatic Energy," +
            "Penguin Somatic Satiety," +
            "Penguin Somatic Activity," +
            "Penguin Somatic Sociability,"
               );
        int count = 0;

        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        foreach (var element in allActs)
        {
            ++count;
            string humanAppraisals = loggingFile.massiveToString(element.Value.getMoralFactorForAuthor());
            string penguinAppraisals = loggingFile.massiveToString(element.Value.getMoralFactorForTarget());
            string penguinBodyFactor = loggingFile.massiveToString(element.Value.getBodyFactorForTarget());
            writePath = LoggingFile.LOGS_PATH + LoggingFile.ALL_ACTS + session.ToString() + ".csv";
            AllActs = new StreamWriter(writePath, true, Encoding.GetEncoding("windows-1251"));
            using (AllActs)
                AllActs.WriteLine(
                    count.ToString() + "," +
                    element.Key + "," +
                    humanAppraisals + "," +
                    penguinAppraisals + "," +
                    penguinBodyFactor + ","
                    );
        }

    }

}
