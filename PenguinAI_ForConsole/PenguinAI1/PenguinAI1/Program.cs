﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;

public class penguinModel
{ 
    const double criticalDelta = 0.8;
    const double criticalValue = 3.0;
    const double mediumValueForDiffNorms = 0.27;

    const double r = 1e-1;
    const double criticalValueForDiffNorms = 0.32;
    const string GO_SLEEP_PENGUIN = "GoSleepPinguin";
    const string GO_EAT_PENGUIN = "GoEatPenguin";
    const string GO_TO_COMMUNICATE = "GoCommunicatePenguin";
    const string GO_PLAY_PENGUIN = "GoPlayPenguin";
    const int necessaryCountOfHumanActions = 7;
    private const string SUBORDINATE = "subordinate";
    private const string FRIEND = "friend";
    private const string ENEMY = "enemy";
    private const string CHIEF = "chief";
    private const string PARTY_GOER = "party-goer";
    private const string BORE = "bore";
    private const string HELLO_PINGUIN = "Human greetings penguin";
    private const string FEED_PINGUIN = "Human feeds penguin";
    private const string THROW_BALL_TO_PINGUIN = "Human throws ball into penguin";
    private const string STROKE_PINGUIN = "Human strokes penguin";
    private const string HIT_PINGUIN = "Human hits penguin";
    private const string GO_SLEEP_PINGUIN = "GoSleepPinguin";
    private const string GO_PLAY_PINGUIN = "GoPlayPenguin";
    private const string GO_EAT_PINGUIN = "GoEatPenguin";
    private const string GO_COMMUNICATE_PINGUIN = "GoCommunicatePenguin";
    private const string IGNORE_HUMAN = "IgnoreHuman";
    private string TURN_HEAD_ON_PENGUIN = "Human looks at penguin";
    private const string POSITIVE_HUMAN = "PositiveHuman";
    private const string POSITIVE = "positive";
    private const string IGNORE = "ignore";
    private const double r1 = 0.3;
    private static bool unstableRelations = true;


    static int countHumanActions = 0;
    static bool processRecoveryOfFeelings = false;
    static bool shouldSetConstantFeeling = false;
    static string humanCharacteristic = "NAN";
    static string penguinCharacteristic = "NAN";

    static Dictionary<string, act> allActs = new Dictionary<string, act>();
    static Dictionary<string, act> positiveAndNegative = new Dictionary<string, act>();
    static Dictionary<string, act> allIndependentActions = new Dictionary<string, act>();
    static HashSet<string> unAvaidableActions = new HashSet<string>();
    static Dictionary<string, double[]> feelingsStates = new Dictionary<string, double[]>();
    static List<Tuple<string, double>> somaticLikelihood = new List<Tuple<string, double>>();
    static List<Tuple<string, double>> biasLikelihood = new List<Tuple<string, double>>();

    static public double[] penguinAppraisals = new double[3];
    static public double[] humanAppraisals = new double[3];
    static public double[] penguinFeelings = new double[3];
    static public double[] humanFeelings = new double[3];
    static public double[] penguinSomaticFactor = new double[5];

    double TimerToEat = 0;
    bool timetoeat = false;

    private const double k = 0.01;


    //private void FixedUpdate()
    //{
    //    for (int i = 0; i < 5; i++)
    //    {
    //        if (penguinBodyFactor[0] < 0.02)
    //            penguinBodyFactor[0] = 0;
    //        if (!GetComponent<penguinAI>().Stop && !GetComponent<penguinAI>().isSleep)
    //            penguinBodyFactor[i] -= 0.0001;

    //    }


    //    foreach (var el in penguinBodyFactor)
    //    {
    //        Debug.Log(el);
    //    }

    //    if (penguinBodyFactor[(int)body.energy] < -1)
    //    {
    //        Debug.Log("f");
    //        if (GetComponent<penguinAI>().isSleep == false)
    //        {
    //            penguinMakeAct(GO_SLEEP_PENGUIN);
    //            GetComponent<penguinAI>().isSleep = true;
    //            GetComponent<penguinAI>().Sleep(new object());
    //        }

    //    }

    //    if (penguinBodyFactor[(int)body.satiety] > -0.3)
    //        timetoeat = false;

    //    if (timetoeat)
    //        TimerToEat += 0.02;
    //    else
    //        TimerToEat = 0;



    //    if (penguinBodyFactor[(int)body.satiety] < -0.3 && TimerToEat % 70 < 0.5)
    //    {
    //        timetoeat = true;
    //        Debug.Log("f");
    //        if (GetComponent<penguinAI>().Stop == false)
    //        {
    //            penguinMakeAct(GO_EAT_PENGUIN);
    //            GetComponent<penguinAI>().PenguinActions("goToBoxWithFish");
    //        }

    //    }


    //    if (penguinBodyFactor[(int)body.activity] < -0.2)
    //    {
    //        Debug.Log("f");
    //        if (GetComponent<penguinAI>().Stop == false)
    //        {
    //            penguinMakeAct(GO_PLAY_PENGUIN);
    //            GetComponent<penguinAI>().PenguinActions("goToBoxWithSnowBalls");
    //        }

    //    }

    //    if (penguinBodyFactor[(int)body.sociability] < -0.15)
    //    {
    //        Debug.Log("f");
    //        if (GetComponent<penguinAI>().Stop == false)
    //        {
    //            penguinMakeAct(GO_TO_COMMUNICATE);
    //            GetComponent<penguinAI>().PenguinActions("GoCommunicatePenguin");
    //        }

    //    }

    //}

    enum body
    {
        pain = 0,
        energy = 1,
        satiety = 2,
        activity = 3,
        sociability = 4
    };

    enum moral
    {
        valence = 0,
        arousal = 1,
        dominance = 2
    };
    class act
    {
        public double[] bodyFactorForTarget;
        public double[] moralFactorForTarget;
        public double[] moralFactorForAuthor;
        public string name;
        public string responseActionOn;
        public string actionAuthor;

        public act()
        {
            bodyFactorForTarget = new double[5];
            moralFactorForTarget = new double[3];
            moralFactorForAuthor = new double[3];
        }
        public void setBodyFactorForTarget(double[] values)
        {
            values.CopyTo(bodyFactorForTarget, 0);
        }
        public void setMoralFactorForTarget(double[] values)
        {
            values.CopyTo(moralFactorForTarget, 0);
        }
        public void setMoralFactorForAuthor(double[] values)
        {
            values.CopyTo(moralFactorForAuthor, 0);
        }

        public void setName(string name)
        {
            this.name = name;
        }
        public void setResponseActionOn(string responseActionOn)
        {
            this.responseActionOn = responseActionOn;
        }

        public void setActionAuthor(string actionAuthor)
        {
            this.actionAuthor = actionAuthor;
        }

        public double[] getBodyFactorForTarget()
        {
            return bodyFactorForTarget;
        }
        public double[] getMoralFactorForTarget()
        {
            return moralFactorForTarget;
        }

        public double[] getMoralFactorForAuthor()
        {
            return moralFactorForAuthor;
        }

        public string getName()
        {
            return name;
        }

        public string getResponseActionOn()
        {
            return responseActionOn;
        }

        public string getActionAuthor()
        {
            return actionAuthor;
        }

    };

    static double[] getNewSomaticFactor(double[] body, double[] action)
    {
        double[] resultBody = new double[body.Length];
        for (int i = 0; i < body.Length; ++i)
        {
            resultBody[i] = body[i] + action[i];
        }
        return resultBody;
    }

    static double[] getNewAppraisals(double[] appraisals, double[] action)
    {
        double[] resultAppraisals = new double[appraisals.Length];
        for (int i = 0; i < appraisals.Length; ++i)
        {
            resultAppraisals[i] = (1.0 - r) * appraisals[i] + r * action[i];
        }
        return resultAppraisals;
    }

    static public void penguinMakeAct(string action)
    {
        penguinAppraisals = getNewAppraisals(penguinAppraisals, allIndependentActions[action].getMoralFactorForTarget());
        humanAppraisals = getNewAppraisals(humanAppraisals, allIndependentActions[action].getMoralFactorForAuthor());
        penguinSomaticFactor = getNewSomaticFactor(penguinSomaticFactor, allIndependentActions[action].getBodyFactorForTarget());
    }
    class Penguin
    {
        private double[] bodyFactor;
        private double[] moralFactor;
        public Penguin()
        {
            bodyFactor = new double[5];
            moralFactor = new double[3];
        }

    };


    public void setupActs()
    {
        unAvaidableActions.Add(HIT_PINGUIN);

        double[] a = new double[3];
        feelingsStates.Add(FRIEND, new double[3]);
        feelingsStates[FRIEND] = new double[] { 0.5, 0, 0 };

        feelingsStates.Add(ENEMY, new double[3]);
        feelingsStates[ENEMY] = new double[] { -0.5, 0, 0 };

        feelingsStates.Add(CHIEF, new double[3]);
        feelingsStates[CHIEF] = new double[] { 0, 0, 0.5 };

        feelingsStates.Add(SUBORDINATE, new double[3]);
        feelingsStates[SUBORDINATE] = new double[] { 0, 0, -0.5 };

        feelingsStates.Add(PARTY_GOER, new double[3]);
        feelingsStates[PARTY_GOER] = new double[] { 0, 0.5, 0 };

        feelingsStates.Add(BORE, new double[3]);
        feelingsStates[BORE] = new double[] { 0, -0.5, 0 };

        allActs = JsonConvert.DeserializeObject<Dictionary<string, act>>(File.ReadAllText(@"Actions.json"));

        //allActs.Add(HELLO_PINGUIN, new act());
        //allActs[HELLO_PINGUIN].setBodyFactorForTarget(new double[] { 0, -0.05, 0, 0.3, 0.5 });
        //allActs[HELLO_PINGUIN].setMoralFactorForTarget(new double[] { 0.5, 0.05, -0.1 });
        //allActs[HELLO_PINGUIN].setMoralFactorForAuthor(new double[] { 0.5, 0.05, -0.15 });

        //allActs.Add(FEED_PINGUIN, new act());
        //allActs[FEED_PINGUIN].setBodyFactorForTarget(new double[] { 0, -0.05, 0.7, -0.1, 0.2 });
        //allActs[FEED_PINGUIN].setMoralFactorForTarget(new double[] { 0.5, 0.05, -0.1 });
        //allActs[FEED_PINGUIN].setMoralFactorForAuthor(new double[] { 0.7, 0.1, 0.1 });

        //allActs.Add(THROW_BALL_TO_PINGUIN, new act());
        //allActs[THROW_BALL_TO_PINGUIN].setBodyFactorForTarget(new double[] { 0.05, -0.2, -0.1, 0.2, 0.3 });
        //allActs[THROW_BALL_TO_PINGUIN].setMoralFactorForTarget(new double[] { 0.1, 0.6, 0.05 });
        //allActs[THROW_BALL_TO_PINGUIN].setMoralFactorForAuthor(new double[] { 0.1, 0.6, 0.05 });

        //allActs.Add(STROKE_PINGUIN, new act());
        //allActs[STROKE_PINGUIN].setBodyFactorForTarget(new double[] { -0.2, 0, 0, 0.1, 0.7 });
        //allActs[STROKE_PINGUIN].setMoralFactorForTarget(new double[] { 0.7, -0.15, -0.15 });
        //allActs[STROKE_PINGUIN].setMoralFactorForAuthor(new double[] { 0.7, -0.15, 0.15 });

        //allActs.Add(HIT_PINGUIN, new act());
        //allActs[HIT_PINGUIN].setBodyFactorForTarget(new double[] { 0.7, -0.2, 0, 0, -0.2 });
        //allActs[HIT_PINGUIN].setMoralFactorForTarget(new double[] { -0.7, -0.15, -0.35 });
        //allActs[HIT_PINGUIN].setMoralFactorForAuthor(new double[] { -0.7, 0.2, 0.4 });

        allIndependentActions.Add(GO_SLEEP_PINGUIN, new act());
        allIndependentActions[GO_SLEEP_PINGUIN].setBodyFactorForTarget(new double[] { -0.2, 2, -0.3, -0.4, -0.1 });
        allIndependentActions[GO_SLEEP_PINGUIN].setMoralFactorForTarget(new double[] { 0.1, -0.8, 0 });
        allIndependentActions[GO_SLEEP_PINGUIN].setMoralFactorForAuthor(new double[] { 0, -0.3, 0 });

        allIndependentActions.Add(GO_PLAY_PINGUIN, new act());
        allIndependentActions[GO_PLAY_PINGUIN].setBodyFactorForTarget(new double[] { 0, -0.1, 0, 0.2, 0.2 });
        allIndependentActions[GO_PLAY_PINGUIN].setMoralFactorForTarget(new double[] { 0.6, 0.3, -0.15 });
        allIndependentActions[GO_PLAY_PINGUIN].setMoralFactorForAuthor(new double[] { 0.4, 0.4, -0.05 });

        allIndependentActions.Add(GO_EAT_PINGUIN, new act());
        allIndependentActions[GO_EAT_PINGUIN].setBodyFactorForTarget(new double[] { 0, -0.05, 0, 0.1, 0.15 });
        allIndependentActions[GO_EAT_PINGUIN].setMoralFactorForTarget(new double[] { 0.1, 0.2, -0.25 });
        allIndependentActions[GO_EAT_PINGUIN].setMoralFactorForAuthor(new double[] { 0.15, 0.1, 0.15 });

        allIndependentActions.Add(GO_COMMUNICATE_PINGUIN, new act());
        allIndependentActions[GO_COMMUNICATE_PINGUIN].setBodyFactorForTarget(new double[] { 0, -0.05, 0, 0.1, 0.3 });
        allIndependentActions[GO_COMMUNICATE_PINGUIN].setMoralFactorForTarget(new double[] { 0.7, 0.2, 0 });
        allIndependentActions[GO_COMMUNICATE_PINGUIN].setMoralFactorForAuthor(new double[] { 0.5, 0.3, 0 });

        positiveAndNegative.Add(IGNORE_HUMAN, new act());
        positiveAndNegative[IGNORE_HUMAN].setBodyFactorForTarget(new double[] { 0, 0, 0, 0, -0.3 });
        positiveAndNegative[IGNORE_HUMAN].setMoralFactorForTarget(new double[] { -0.3, -0.3, 0.15 });
        positiveAndNegative[IGNORE_HUMAN].setMoralFactorForAuthor(new double[] { -0.4, -0.2, -0.3 });

        positiveAndNegative.Add(POSITIVE_HUMAN, new act());
        positiveAndNegative[POSITIVE_HUMAN].setBodyFactorForTarget(new double[] { 0, 0, 0, 0.1, 0.1 });
        positiveAndNegative[POSITIVE_HUMAN].setMoralFactorForTarget(new double[] { 0.12, 0.05, 0.1 });
        positiveAndNegative[POSITIVE_HUMAN].setMoralFactorForAuthor(new double[] { 0.35, 0.15, -0.1 });
    }

    static void isRelationsUnstable(double [] feelings)
    {
        string humanCharacteristic = findHumanFeelingCharacteristick(feelings);
        double dif = 10;
        for (int i = 0; i < 3; ++i)
        {
            if (feelingsStates[humanCharacteristic][i] != 0)
            {
                dif = Math.Min(Math.Abs(feelings[i] - feelingsStates[humanCharacteristic][i]), dif);
            }
        }
        if (dif < 0.15)
        {
            unstableRelations = false;
        }
    }

    static double[] getNewFeelingFactor(double[] feelings, double[] appraisals)
    {
        isRelationsUnstable(feelings);
        if (unstableRelations)
        {
            feelings = firstMethorRebuildFeelings(feelings, appraisals);
            return feelings;
        }
        double diffNorm = 0;
        for (int i = 0; i < 3; ++i)
        {
            diffNorm += Math.Abs(feelings[i] - appraisals[i]);
        }
        processRecoveryOfFeelings = diffNorm > criticalValueForDiffNorms;
        if (processRecoveryOfFeelings)
        {
            feelings = secondMethodRebuildFeelings(feelings, appraisals);
        } else
        {
            feelings = setConstantFeelings(feelings);
        }
        //System.Console.WriteLine(feelings[0] + "rrrr " + feelings[1] + "rrr " + feelings[2]);
        return feelings;
    }

    static double[] firstMethorRebuildFeelings(double[] feelings, double[] appraisals)
    {
        double[] resultFeelings = new double[feelings.Length];
        for (int i = 0; i < 3; i++)
        {
            resultFeelings[i] = 1.1 * appraisals[i];
        }
        return resultFeelings;
    }

    static double[] secondMethodRebuildFeelings(double[] feelings, double[] appraisals)
    {
        Console.WriteLine("secondMethodRebuildFeelings");
        double[] resultFeelings = new double[feelings.Length];
        for (int i = 0; i < 3; ++i)
        {
            resultFeelings[i] = (1 - r1) * feelings[i] + r1 * (appraisals[i] - feelings[i]);
        }
        humanCharacteristic = "NAN";
        return resultFeelings;
    }

    static string findHumanFeelingCharacteristick(double [] feelings)
    {
        string choise = "";
        double dif = 20;
        foreach (var feelingMassive in feelingsStates)
        {
            for (int i = 0; i < 3; ++i)
            {
                if (feelingMassive.Value[i] != 0)
                {
                    double mid = Math.Abs(feelingMassive.Value[i] - feelings[i]);
                    if (mid < dif)
                    {
                        dif = mid;
                        choise = feelingMassive.Key;
                    }
                }
            }
        }
        return choise;
    }

    static double[] setConstantFeelings(double[] feelings)
    {
        Console.WriteLine("setConstantFeelings");
        humanCharacteristic = findHumanFeelingCharacteristick(feelings);
        double[] ans = new double[3];
        feelingsStates[humanCharacteristic].CopyTo(ans, 0);
        return ans;
    }
    static void somaticCriterion(double[] penguinBodyFactor, string action)
    {
        double maxValue = 0;
        double mainNorm = 0;
        double recNorm = 0;
        foreach (var characterAction in allActs)
        {
            double normAfterAction = 0;
            var currentAction = characterAction.Value;
            if (currentAction.getResponseActionOn() == action)
            {
                for (int i = 0; i < penguinBodyFactor.Length; ++i)
                {
                    normAfterAction += Math.Pow(penguinBodyFactor[i] + currentAction.getBodyFactorForTarget()[i], 2);
                }
                somaticLikelihood.Add(new Tuple<string, double>(currentAction.getName(), normAfterAction));
                mainNorm += normAfterAction;
                maxValue = Math.Max(maxValue, Math.Abs(normAfterAction));
            }
        }
        for (int i = 0; i < somaticLikelihood.Count; ++i)
        {
            somaticLikelihood[i] = new Tuple<string, double>(somaticLikelihood[i].Item1, 1 - somaticLikelihood[i].Item2/mainNorm);
            recNorm += somaticLikelihood[i].Item2;
            //somaticLikelihood[i] = new Tuple<string, double>(somaticLikelihood[i].Item1, maxValue - somaticLikelihood[i].Item2);
        }
        for (int i = 0; i < somaticLikelihood.Count; ++i)
        {
            somaticLikelihood[i] = new Tuple<string, double>(somaticLikelihood[i].Item1, somaticLikelihood[i].Item2 / recNorm);
            System.Console.WriteLine("somatic norms for " + somaticLikelihood[i].Item1 + " " + somaticLikelihood[i].Item2);
        }
    }

    static void biasCriterion(double[] appraisalsFactor, double[] feelingsFactor, string action)
    {
        double maxValue = 0;
        double mainNorm = 0;
        double recNorm = 0;
        foreach (var el in allActs)
        {
            double difference = 0;
            var responseAction = el.Value;
            if (responseAction.getResponseActionOn() == action)
            {
                var appraisalsAfterAction = getNewAppraisals(appraisalsFactor, responseAction.getMoralFactorForAuthor());
                for (int i = 0; i<feelingsFactor.Length; ++i)
                {
                    difference += Math.Pow(feelingsFactor[i] - appraisalsAfterAction[i], 2);
                }
                biasLikelihood.Add(new Tuple<string, double>(responseAction.getName(), difference));
                mainNorm += difference;
                maxValue = Math.Max(maxValue, difference);
            }
        }
        for (int i=0; i < biasLikelihood.Count; ++i)
        {
            biasLikelihood[i] = new Tuple<string, double>(biasLikelihood[i].Item1, 1 - biasLikelihood[i].Item2/mainNorm);
            recNorm += biasLikelihood[i].Item2;
            // biasLikelihood[i] = new Tuple<string, double>(biasLikelihood[i].Item1, maxValue - biasLikelihood[i].Item2);
        }
        for (int i = 0; i < biasLikelihood.Count; ++i)
        {
            biasLikelihood[i] = new Tuple<string, double>(biasLikelihood[i].Item1, biasLikelihood[i].Item2 / recNorm);
            System.Console.WriteLine("bias norms for " + biasLikelihood[i].Item1 + " " + biasLikelihood[i].Item2);
        }
    }

    void rebuildStatesAfterHumanAction(string humanAct)
    {
        humanAppraisals = getNewAppraisals(humanAppraisals, allActs[humanAct].getMoralFactorForAuthor()); // изменения состояния человека после действия
        humanFeelings = getNewFeelingFactor(humanFeelings, humanAppraisals);

        penguinAppraisals = getNewAppraisals(penguinAppraisals, allActs[humanAct].getMoralFactorForTarget()); 
        penguinSomaticFactor = getNewSomaticFactor(penguinSomaticFactor, allActs[humanAct].getBodyFactorForTarget());
        //penguinFeelings = getNewFeelingFactor(penguinFeelings, penguinAppraisals); //изменения состояния пингвина после действия
        System.Console.WriteLine("humanCharacteristic = " + humanCharacteristic);
        mitAppraisalsAndFeelings();
    }
   
    void rebuildStatesAfterPenguinAction(string penguinAct)
    {
        humanAppraisals = getNewAppraisals(humanAppraisals, allActs[penguinAct].getMoralFactorForTarget()); // изменения состояния человека после действия
        humanFeelings = getNewFeelingFactor(humanFeelings, humanAppraisals);

        penguinAppraisals = getNewAppraisals(penguinAppraisals, allActs[penguinAct].getMoralFactorForAuthor());
        penguinSomaticFactor = getNewSomaticFactor(penguinSomaticFactor, allActs[penguinAct].getBodyFactorForTarget());
        //penguinFeelings = getNewFeelingFactor(penguinFeelings, penguinAppraisals); //изменения состояния пингвина после действия
        System.Console.WriteLine("humanCharacteristic = " + humanCharacteristic);
        mitAppraisalsAndFeelings();
    }

    public string getResponseAction()
    {
        string response = "";
        List<Tuple<string, double>> result = new List<Tuple<string, double>>();
        foreach (var somaticEl in somaticLikelihood)
        {
            foreach (var biasEl in biasLikelihood)
            {
                if (somaticEl.Item1 == biasEl.Item1)
                {
                    result.Add(new Tuple<string, double>(somaticEl.Item1, biasEl.Item2 + somaticEl.Item2 * k));
                }
            }
        }
        double actionLikelihood = 0;
        foreach (var el in result)
        {
            if (el.Item2 > actionLikelihood)
            {
                actionLikelihood = el.Item2;
                response = el.Item1;
            }
        }
        return response;
    }



    public string getResponseActionByLikelihood()
    {
        string response = "";
        List<Tuple<string, double>> result = new List<Tuple<string, double>>();
        double sum = 0;
        foreach (var somaticEl in somaticLikelihood)
        {
            foreach (var biasEl in biasLikelihood)
            {
                if (somaticEl.Item1 == biasEl.Item1)
                {
                    double likelihood = biasEl.Item2 + somaticEl.Item2 * k;
                    result.Add(new Tuple<string, double>(somaticEl.Item1, likelihood));
                    sum += likelihood;
                }
            }
        }
        for (int i = 0; i<result.Count; ++i)
        {
            result[i] = new Tuple<string, double>(result[i].Item1, result[i].Item2 / sum);
        }
        result.Sort((x, y) => x.Item2.CompareTo(y.Item2));
        System.Random x = new System.Random();
        double actionLikelihood = Convert.ToDouble(x.Next(0, 10000) / 10000.0);
        double currentLikelihood = 0;
        foreach (var el in result)
        {
            currentLikelihood += el.Item2;
           // System.Console.WriteLine("actionLikelihood " + actionLikelihood + " " + currentLikelihood + " " +el.Item2);
            if (currentLikelihood > actionLikelihood)
            {
                response = el.Item1;
                break;
            }
        }
        return response;
    }

    double [] limitVecs(double [] vec)
    {
        for (int i = 0; i < vec.Length; ++i)
        {
            vec[i] = Math.Min(vec[i], 0.5);
            vec[i] = Math.Max(vec[i], -0.5);
        }
        return vec;
    }

    void mitAppraisalsAndFeelings()
    {
        limitVecs(humanAppraisals).CopyTo(humanAppraisals, 0);
        limitVecs(humanFeelings).CopyTo(humanFeelings, 0);
        limitVecs(penguinAppraisals).CopyTo(penguinAppraisals, 0);
        limitVecs(penguinFeelings).CopyTo(penguinFeelings, 0);
    }

    public string gettedExternalEffect(string humanAct)
    {
        somaticLikelihood = new List<Tuple<string, double>>();
        biasLikelihood = new List<Tuple<string, double>>();

        rebuildStatesAfterHumanAction(humanAct);
        somaticCriterion(penguinSomaticFactor, humanAct);
        biasCriterion(humanAppraisals, humanFeelings, humanAct);
        string answer = getResponseActionByLikelihood();
        rebuildStatesAfterPenguinAction(answer);
        return answer;
    }

    public void printArray(double [] array)
    {
        foreach(double el in array)
        {
            System.Console.Write(el + " ");
        }
        System.Console.WriteLine();
    }

    public void printAllLogs(string reaction )
    {
        System.Console.WriteLine("Penguin reaction = " + reaction);      
        System.Console.Write("penguinAppraisals ");
        printArray(penguinAppraisals);
        System.Console.Write("humanAppraisals ");
        printArray(humanAppraisals);
        System.Console.Write("penguinFeelings ");
        printArray(penguinFeelings);
        System.Console.Write("humanFeelings ");
        printArray(humanFeelings);
        System.Console.Write("penguinBodyFactor ");
        printArray(penguinSomaticFactor);
    }

    public penguinModel()
    {
        string[] arr = new string[7];
        arr[0] = HELLO_PINGUIN;
        arr[1] = FEED_PINGUIN;
        arr[2] = THROW_BALL_TO_PINGUIN;
        arr[3] = STROKE_PINGUIN;
        arr[4] = HIT_PINGUIN;
        arr[5] = TURN_HEAD_ON_PENGUIN;
        arr[6] = "EXIT";
        setupActs();
        while (true)
        {
            for (int i = 0; i<arr.Length; ++i)
            {
                System.Console.WriteLine(i + " " + arr[i]);
            }
            System.Console.WriteLine();
            string inputAction;        
            inputAction = System.Console.ReadLine();
            int value = Convert.ToInt32(inputAction);
            if (value < 0 || value >= arr.Length)
            {
                continue;
            }
            System.Console.WriteLine(inputAction);
            if (value == arr.Length - 1)
            {
                break;
            }
            string reaction = gettedExternalEffect(arr[value]);
            printAllLogs(reaction);
        }
    }

    static void Main(string[] args)
    {

        int count = 100;
        int sum = 0;
        int up80 = 0;
        int down20 = 0;
        while (count-->0)
        {

            int delta = 0;
            int actions = (int)1e6;
            System.Random x = new System.Random();
            while (actions-- > 0)
            {
                double actionLikelihood = Convert.ToDouble(x.Next(0, 10000) / 10000.0);
                if (actionLikelihood > 0.5)
                {
                    delta++;
                }
                else
                {
                    delta--;
                }
                if (actionLikelihood > 0.7)
                {
                    up80++;
                }
                if (actionLikelihood < 0.3)
                {
                    down20++;
                }

            }
            sum += delta;
            System.Console.WriteLine(delta);
        }
        System.Console.WriteLine((double)down20/up80);
        System.Console.WriteLine();
        System.Console.WriteLine(sum);
        new penguinModel();
        
    }
} 